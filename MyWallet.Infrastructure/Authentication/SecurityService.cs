﻿using System;
using System.Diagnostics;
using System.Security.Cryptography;

namespace MyWallet.Infrastructure.Authentication
{
    public class SecurityService
    {
        public const int SALT_BYTES = 24;
        public const int HASH_BYTES = 18;
        public const int PBKDF2_ITERATIONS = 64000;

        public byte[] CreateHash(string password, byte[] salt)
        {
            Debug.WriteLine("creating pwd from " + password);
            var pwdBytes = GetBytes(password);
            return PBKDF2(pwdBytes, salt, PBKDF2_ITERATIONS, HASH_BYTES);
        }

        public byte[] GetSalt()
        {
            var salt = new byte[SALT_BYTES];

            try
            {
                using (var randomGenerator = new RNGCryptoServiceProvider())
                    randomGenerator.GetBytes(salt);
                return salt;
            }
            catch (CryptographicException ex)
            {
                throw new InvalidProgramException("Random number generator not available.", ex);
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentException("Invalid argument given to random number generator.", ex);
            }
        }

        public bool VerifyPassword(string password, byte[] goodHash, byte[] salt)
        {
            return SlowEquals(goodHash, CreateHash(password, salt));
        }

        private bool SlowEquals(byte[] a, byte[] b)
        {
            uint diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
                diff |= (uint)(a[i] ^ b[i]);
            return diff == 0;
        }

        private byte[] PBKDF2(byte[] password, byte[] salt, int iterations, int outputBytes)
        {
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt, iterations))
                return pbkdf2.GetBytes(outputBytes);
        }

        private byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
