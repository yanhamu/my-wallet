﻿using Dapper;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace MyWallet.Infrastructure.Authentication
{
    public class UserRepository
    {
        private readonly SqlConnection sqlConnection;

        public UserRepository(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
        }

        public Task<UserDefaultAuthentication> GetByUsername(string username)
        {
            var query = @"
select username, hashedpassword, salt, userId
from UserDefaultAuthentication 
where username = @username";

            return sqlConnection
                .QuerySingleOrDefaultAsync<UserDefaultAuthentication>(query, new { username = username });
        }
    }

    public class UserDefaultAuthentication
    {
        public string Username { get; set; }
        public byte[] HashedPassword { get; set; }
        public byte[] Salt { get; set; }
        public Guid UserId { get; set; }
    }
}
