﻿using AutoMapper;
using Model = MyWallet.Api.Model;

namespace MyWallet.Infrastructure
{
    public static class AutoMapperInitializer
    {
        public static IMapper Initialize()
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.Transaction, Model.TransactionHeader>()
                    .ForMember(d => d.AccountId, m => m.MapFrom(s => s.AccountId))
                    .ForMember(d => d.Amount, m => m.MapFrom(s => s.Amount))
                    .ForMember(d => d.CategoryId, m => m.MapFrom(s => s.CategoryId))
                    .ForMember(d => d.Id, m => m.MapFrom(s => s.Id));

                cfg.CreateMap<Domain.Transaction, Model.Transaction>()
                    .ForMember(d => d.AccountId, m => m.MapFrom(s => s.AccountId))
                    .ForMember(d => d.Amount, m => m.MapFrom(s => s.Amount))
                    .ForMember(d => d.CategoryId, m => m.MapFrom(s => s.CategoryId))
                    .ForMember(d => d.Id, m => m.MapFrom(s => s.Id))
                    .ForMember(d => d.Description, m => m.MapFrom(s => s.Description))
                    .ForMember(d => d.MovementDate, m => m.MapFrom(s => s.MovementDate))
                    .Include<Domain.FioTransaction, Model.FioTransaction>();

                cfg.CreateMap<Domain.FioTransaction, Model.FioTransaction>()
                    .ForMember(d => d.AccountId, m => m.MapFrom(s => s.AccountId))
                    .ForMember(d => d.Amount, m => m.MapFrom(s => s.Amount))
                    .ForMember(d => d.CategoryId, m => m.MapFrom(s => s.CategoryId))
                    .ForMember(d => d.Id, m => m.MapFrom(s => s.Id))
                    .ForMember(d => d.Description, m => m.MapFrom(s => s.Description))
                    .ForMember(d => d.MovementDate, m => m.MapFrom(s => s.MovementDate))
                    .ForMember(d => d.MovementId, m => m.MapFrom(s => s.MovementId))
                    .ForMember(d => d.Currency, m => m.MapFrom(s => s.Currency))
                    .ForMember(d => d.ContraAccount, m => m.MapFrom(s => s.ContraAccount))
                    .ForMember(d => d.ContraAccountName, m => m.MapFrom(s => s.ContraAccountName))
                    .ForMember(d => d.ContraAccountBankCode, m => m.MapFrom(s => s.ContraAccountBankCode))
                    .ForMember(d => d.ContraAccountBankName, m => m.MapFrom(s => s.ContraAccountBankName))
                    .ForMember(d => d.ConstantSymbol, m => m.MapFrom(s => s.ConstantSymbol))
                    .ForMember(d => d.VariableSymbol, m => m.MapFrom(s => s.VariableSymbol))
                    .ForMember(d => d.SpecificSymbol, m => m.MapFrom(s => s.SpecificSymbol))
                    .ForMember(d => d.UserIdentification, m => m.MapFrom(s => s.UserIdentification))
                    .ForMember(d => d.MessageForReceiver, m => m.MapFrom(s => s.MessageForReceiver))
                    .ForMember(d => d.TransactionType, m => m.MapFrom(s => s.TransactionType))
                    .ForMember(d => d.TransactionExecutive, m => m.MapFrom(s => s.TransactionExecutive))
                    .ForMember(d => d.Specification, m => m.MapFrom(s => s.Specification))
                    .ForMember(d => d.Comment, m => m.MapFrom(s => s.Comment))
                    .ForMember(d => d.BankIndentifierCode, m => m.MapFrom(s => s.BankIndentifierCode))
                    .ForMember(d => d.TransactionId, m => m.MapFrom(s => s.TransactionId));

                cfg.CreateMap<Domain.Category, Model.Category>()
                    .ForMember(d => d.Color, m => m.MapFrom(s => s.Color))
                    .ForMember(d => d.Id, m => m.MapFrom(s => s.Id))
                    .ForMember(d => d.IsDefault, m => m.MapFrom(s => s.IsDefault))
                    .ForMember(d => d.Name, m => m.MapFrom(s => s.Name))
                    .ForMember(d => d.ParentId, m => m.MapFrom(s => s.ParentId));

                cfg.CreateMap<Domain.Category, Model.HierarchicalCategory>()
                   .ForMember(d => d.Color, m => m.MapFrom(s => s.Color))
                   .ForMember(d => d.Id, m => m.MapFrom(s => s.Id))
                   .ForMember(d => d.IsDefault, m => m.MapFrom(s => s.IsDefault))
                   .ForMember(d => d.Name, m => m.MapFrom(s => s.Name))
                   .ForMember(d => d.ParentId, m => m.MapFrom(s => s.ParentId))
                   .ForMember(d => d.Children, m => m.MapFrom(s => s.Children));

                cfg.CreateMap<Domain.Account, Model.Account>()
                    .ForMember(d => d.Id, m => m.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));

                cfg.CreateMap<Domain.Account, Model.Account>()
                    .ForMember(d => d.Id, m => m.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, m => m.MapFrom(s => s.Name));

                cfg.CreateMap<Domain.FioAccount, Model.FioAccount>()
                    .ForMember(d => d.Id, m => m.MapFrom(s => s.Id))
                    .ForMember(d => d.LastSyncDate, m => m.MapFrom(s => s.LastSyncDate))
                    .ForMember(d => d.Name, m => m.MapFrom(s => s.Name))
                    .ForMember(d => d.Token, m => m.MapFrom(s => s.Token));

                cfg.CreateMap<Domain.User, Model.User>()
                    .ForMember(d => d.FirstName, m => m.MapFrom(s => s.FirstName))
                    .ForMember(d => d.Id, m => m.MapFrom(s => s.Id))
                    .ForMember(d => d.LastName, m => m.MapFrom(s => s.LastName));
            });
            return mapper.CreateMapper();
        }
    }
}
