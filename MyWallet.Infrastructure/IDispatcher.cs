﻿using MyWallet.Infrastructure.Commands;

namespace MyWallet.Infrastructure
{
    public interface IDispatcher
    {
        void Dispatch<TCommand>(TCommand command) where TCommand : ICommand;
    }
}
