﻿using MyWallet.Api.Filters;
using MyWallet.Filters;
using System.Web.Http.Filters;

namespace MyWallet.App_Start
{
    public static class FilterBootstrap
    {
        public static void Bootstrap(HttpFilterCollection filters)
        {
            filters.Add(new ValidateModelAttribute());
            filters.Add(new ValidateNullModelAttribute());
            filters.Add(new AccountExistsAttribute());
            filters.Add(new TransactionExistsAttribute());
            filters.Add(new AuthorizedUserAccountAttribute());
            filters.Add(new CategoryExistsAttribute());
            filters.Add(new AuthorizedUserCategoryAttribute());
            filters.Add(new AccountTransactionConsistentAttribute());
        }
    }
}