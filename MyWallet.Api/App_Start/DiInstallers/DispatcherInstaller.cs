﻿using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MyWallet.Infrastructure;
using MyWallet.Infrastructure.Commands;

namespace MyWallet.App_Start.DiInstallers
{
    public class DispatcherInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IDispatcher>().ImplementedBy<MyDispatcher>().LifestyleTransient());
        }

        private class MyDispatcher : IDispatcher
        {
            private readonly IKernel kernel;

            public MyDispatcher(IKernel kernel)
            {
                this.kernel = kernel;
            }
            public void Dispatch<TCommand>(TCommand command) where TCommand : ICommand
            {
                var handlers = kernel.ResolveAll<ICommandHandler<TCommand>>();
                foreach (var h in handlers)
                {
                    h.Handle(command);
                }
            }
        }
    }
}