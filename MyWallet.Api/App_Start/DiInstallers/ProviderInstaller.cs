﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MyWallet.Service.CategoryProviders;

namespace MyWallet.App_Start.DiInstallers
{
    public class ProviderInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<ICategoryProvider>()
                .Where(c => c.Name.EndsWith("Provider"))
                .WithServiceAllInterfaces()
                .LifestyleTransient());
        }
    }
}