﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MyWallet.DataAccess;

namespace MyWallet.App_Start.DiInstallers
{
    public class DbContextInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<MyWalletDbContext>().LifestylePerWebRequest());
        }
    }
}