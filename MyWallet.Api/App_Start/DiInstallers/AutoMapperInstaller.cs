﻿using AutoMapper;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MyWallet.Infrastructure;

namespace MyWallet.App_Start.DiInstallers
{
    public class AutoMapperInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IMapper>()
                    .UsingFactoryMethod(f => AutoMapperInitializer.Initialize())
                    .LifestyleSingleton());
        }
    }
}