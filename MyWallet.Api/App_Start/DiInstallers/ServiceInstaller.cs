﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MyWallet.Fio.DataAccess;
using MyWallet.Service.AccountSynchronization;

namespace MyWallet.App_Start.DiInstallers
{
    public class ServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<FioAccountConnector>()
                    .DependsOn(Dependency.OnValue("fioBaseUrl", "https://www.fio.cz/ib_api/rest"))
                    .LifestyleTransient(),
                Component
                    .For<FioAdapter>()
                    .LifestyleTransient());
        }
    }
}