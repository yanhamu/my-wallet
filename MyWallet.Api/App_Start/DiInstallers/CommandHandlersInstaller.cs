﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MyWallet.Infrastructure.Commands;
using MyWallet.Service.Authentication;

namespace MyWallet.App_Start.DiInstallers
{
    public class CommandHandlersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes
                    .FromAssemblyContaining<CreateUserDefaultAuthenticationCommandHandler>()
                    .BasedOn(typeof(ICommandHandler<>))
                    .Unless(t => t == typeof(MyWallet.Service.Handlers.TransactionalCommandHandler<>))
                    .WithService
                    .Base()
                    .LifestyleTransient());
        }
    }
}