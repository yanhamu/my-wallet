﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MyWallet.Api.Model.Filters;
using MyWallet.Api.Model.QueryBuilders;
using MyWallet.Domain;
using MyWallet.Infrastructure.Authentication;
using MyWallet.Service.Accounts;
using MyWallet.Service.Transactions.QueryFilters;

namespace MyWallet.App_Start.DiInstallers
{
    public class QueryServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<SecurityService>().LifestyleTransient(),
                Classes
                    .FromAssemblyContaining<AccountQueryService>()
                    .Where(s => s.Name.EndsWith("Service"))
                    .WithServiceAllInterfaces()
                    .LifestyleTransient(),

                Component
                    .For<IQueryBuilder<TransactionFilter, Transaction>>()
                    .ImplementedBy<SpottedFilter>()
                    .LifestyleTransient(),

                Component
                    .For<IQueryBuilder<TransactionFilter, Transaction>>()
                    .ImplementedBy<CategoryFilter>()
                    .LifestyleTransient()
                    );
        }
    }
}