﻿using Microsoft.Owin.Security.OAuth;
using MyWallet.Infrastructure.Authentication;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MyWallet.App_Start
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyWallet"].ConnectionString))
            {
                var userRepository = new UserRepository(sqlConnection);
                var user = await userRepository.GetByUsername(context.UserName);
                if (user == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }

                if (!new SecurityService().VerifyPassword(context.Password, user.HashedPassword, user.Salt))
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }
                identity.AddClaim(new Claim("userId", user.UserId.ToString()));
            }

            identity.AddClaim(new Claim("sub", context.UserName));
            identity.AddClaim(new Claim("role", "user"));

            context.Validated(identity);
        }
    }
}