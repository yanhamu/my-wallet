﻿using MyWallet.Domain;
using MyWallet.Filters;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace MyWallet.Api.Filters
{
    public class TransactionExistsAttribute : ActionFilterWithServiceLocator
    {
        public string ParameterName { get { return "transactionId"; } }

        public override void OnActionExecuting(HttpActionContext context)
        {
            if (!context.ActionArguments.ContainsKey(ParameterName))
            {
                return;
            }

            var id = GetParameter<Guid>(context, ParameterName);
            var repository = GetRepository<Transaction>(context);

            var transaction = repository.Find(id);
            if (transaction == null)
                context.Response = context.Request.CreateResponse(HttpStatusCode.NotFound, "transaction not found");
        }
    }
}