﻿using MyWallet.Domain;
using MyWallet.Filters;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace MyWallet.Api.Filters
{
    public class AccountTransactionConsistentAttribute : ActionFilterWithServiceLocator
    {
        private readonly string accountParamName;
        private readonly string transactionParamName;

        public AccountTransactionConsistentAttribute()
        {
            this.accountParamName = "accountId";
            this.transactionParamName = "transactionId";
        }

        public override void OnActionExecuting(HttpActionContext context)
        {
            if (!context.ActionArguments.ContainsKey(accountParamName) || !context.ActionArguments.ContainsKey(transactionParamName))
            {
                return;
            }

            var transactionId = GetParameter<Guid>(context, transactionParamName);
            var accountId = GetParameter<Guid>(context, accountParamName);
            var repository = GetRepository<Transaction>(context);

            var transaction = repository.Find(transactionId);
            if (transaction.AccountId != accountId)
                context.Response = context.Request.CreateResponse(HttpStatusCode.NotFound, "transaction/account not found");
        }
    }
}