﻿using MyWallet.DataAccess;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace MyWallet.Filters
{
    public abstract class ActionFilterWithServiceLocator : ActionFilterAttribute
    {

        public T GetDependency<T>(HttpActionContext context)
        {
            return (T)context.ControllerContext.Configuration.DependencyResolver.GetService(typeof(T));
        }
        public T GetParameter<T>(HttpActionContext context, string parameterName)
        {
            return (T)context.ActionArguments[parameterName];
        }

        public Repository<T> GetRepository<T>(HttpActionContext context) where T : class
        {
            return GetDependency<Repository<T>>(context);
        }

    }
}