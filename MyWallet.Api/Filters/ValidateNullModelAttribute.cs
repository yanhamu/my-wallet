﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace MyWallet.Api.Filters
{
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class ValidateNullModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var isNullable = actionContext
                .ActionDescriptor
                .GetCustomAttributes<NullableParameterAttribute>()
                .Any();

            if (!isNullable && actionContext.ActionArguments.ContainsValue(null))
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "argument cannot be null");
        }
    }
}