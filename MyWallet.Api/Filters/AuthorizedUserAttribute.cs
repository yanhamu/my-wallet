﻿using MyWallet.Controllers;
using System;
using System.Web.Http.Controllers;

namespace MyWallet.Filters
{
    public abstract class AuthorizedUserAttribute : ActionFilterWithServiceLocator
    {
        private readonly string parameterName;

        public AuthorizedUserAttribute(string parameterName = "id")
        {
            this.parameterName = parameterName;
        }

        public Guid GetUserId(HttpActionContext context)
        {
            return ((BaseController)(context.ControllerContext.Controller)).UserId;
        }
    }
}