﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace MyWallet.Filters.Exceptions
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private readonly Type exceptionType;
        private readonly string message;
        private readonly HttpStatusCode statusCode;

        public ExceptionFilter(Type exceptionType, HttpStatusCode statusCode, string message)
        {
            this.exceptionType = exceptionType;
            this.statusCode = statusCode;
            this.message = message;
        }

        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception.GetType() == exceptionType)
                context.Response = context.Request.CreateResponse(statusCode, message);
        }
    }
}