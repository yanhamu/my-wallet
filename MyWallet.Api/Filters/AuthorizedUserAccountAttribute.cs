﻿using MyWallet.Domain;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace MyWallet.Filters
{
    public class AuthorizedUserAccountAttribute : AuthorizedUserAttribute
    {
        public string ParameterName { get { return "accountId"; } }

        public override void OnActionExecuting(HttpActionContext context)
        {
            if (!context.ActionArguments.ContainsKey(ParameterName))
            {
                return;
            }

            var id = GetParameter<Guid>(context, ParameterName);
            var repository = GetRepository<Account>(context);

            var account = repository.Find(id);
            var owns = account.UserId == GetUserId(context);

            if (owns == false)
                context.Response = context.Request.CreateResponse(HttpStatusCode.NotFound);
        }
    }
}