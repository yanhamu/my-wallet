﻿using System;

namespace MyWallet.Api.Filters
{
    /// <summary>
    /// Marks method for argument validator to ignore null values.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class NullableParameterAttribute : Attribute { }
}