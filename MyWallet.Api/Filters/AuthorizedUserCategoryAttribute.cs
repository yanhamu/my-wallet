﻿using MyWallet.Domain;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace MyWallet.Filters
{
    public class AuthorizedUserCategoryAttribute : AuthorizedUserAttribute
    {
        private readonly string parameterName;

        public AuthorizedUserCategoryAttribute()
        {
            this.parameterName = "categoryId";
        }

        public override void OnActionExecuting(HttpActionContext context)
        {
            if (!context.ActionArguments.ContainsKey(parameterName))
            {
                return;
            }

            var id = GetParameter<Guid>(context, parameterName);
            var repository = GetRepository<Category>(context);

            var category = repository.Find(id);
            var owns = category.UserId == GetUserId(context);

            if (owns == false)
                context.Response = context.Request.CreateResponse(HttpStatusCode.NotFound);
        }
    }
}