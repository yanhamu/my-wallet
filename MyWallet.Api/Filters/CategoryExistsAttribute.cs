﻿using MyWallet.Domain;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace MyWallet.Filters
{
    public class CategoryExistsAttribute : ActionFilterWithServiceLocator
    {
        private string ParameterName { get; set; }

        public CategoryExistsAttribute()
        {
            this.ParameterName = "categoryId";
        }

        public override void OnActionExecuting(HttpActionContext context)
        {
            if (!context.ActionArguments.ContainsKey(ParameterName))
            {
                return;
            }

            var id = GetParameter<Guid>(context, ParameterName);
            var repository = GetRepository<Category>(context);

            var category = repository.Find(id);
            if (category == null)
                context.Response = context.Request.CreateResponse(HttpStatusCode.NotFound, "category not found");
        }
    }
}