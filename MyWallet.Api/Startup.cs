﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using MyWallet.App_Start;
using MyWallet.DataAccess;
using Owin;
using System;
using System.Web.Http;

[assembly: OwinStartup(typeof(MyWallet.Startup))]
namespace MyWallet
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);
            DatabaseInitializer.Initialize();

            var config = new HttpConfiguration();
            config.DependencyResolver = new WindsorHttpDependencyResolver(Bootstrapper.CreateContainer());

            Bootstrapper.ConfigureJsonSerializer(config.Formatters.JsonFormatter.SerializerSettings);
            FilterBootstrap.Bootstrap(config.Filters);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);

            config.MapHttpAttributeRoutes();
            config.EnsureInitialized();
        }

        private void ConfigureOAuth(IAppBuilder app)
        {
            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/getsecuretoken"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new AuthorizationServerProvider()
            };

            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}