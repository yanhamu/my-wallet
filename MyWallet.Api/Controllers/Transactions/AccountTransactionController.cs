﻿using MyWallet.Api.Model;
using MyWallet.Infrastructure;
using MyWallet.Service.Transactions;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyWallet.Controllers.Transactions
{
    [Authorize]
    [RoutePrefix("api/accounts/{accountId:Guid}/transactions")]
    public class AccountTransactionController : BaseController
    {
        private readonly TransactionQueryService queryService;
        private readonly IDispatcher dispatcher;

        public AccountTransactionController(
            IDispatcher dispatcher,
            TransactionQueryService transactionService)
        {
            this.dispatcher = dispatcher;
            this.queryService = transactionService;
        }

        [Route]
        [HttpGet]
        public HttpResponseMessage GetAll(Guid accountId)
        {
            var transactions = queryService.GetAll(accountId);
            return Request.CreateResponse(HttpStatusCode.OK, transactions);
        }

        [Route]
        [HttpPost]
        public HttpResponseMessage Create(Transaction model, Guid accountId)
        {
            var id = Guid.NewGuid();

            var command = new CreateTransactionCommand()
            {
                AccountId = accountId,
                Amount = model.Amount,
                Id = id,
                MovementDate = model.MovementDate == default(DateTime)
                    ? DateTime.UtcNow
                    : model.MovementDate,
                CategoryId = model.CategoryId,
                UserId = UserId,
                WasSpotted = model.WasSpotted
            };

            dispatcher.Dispatch(command);

            return Request.CreateResponse(HttpStatusCode.OK, queryService.GetById(id));
        }
    }
}