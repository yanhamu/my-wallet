﻿using MyWallet.Api.Filters;
using MyWallet.Api.Model;
using MyWallet.Api.Model.Filters;
using MyWallet.Infrastructure;
using MyWallet.Service.Transactions;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyWallet.Controllers.Transactions
{
    [Authorize]
    [RoutePrefix("api/transactions")]
    public class TransactionController : BaseController
    {
        private readonly IDispatcher dispatcher;
        private readonly TransactionQueryService queryService;

        public TransactionController(
            IDispatcher dispatcher,
            TransactionQueryService queryService)
        {
            this.dispatcher = dispatcher;
            this.queryService = queryService;
        }

        [Route]
        [HttpGet]
        [NullableParameter]
        public HttpResponseMessage Get([FromUri]TransactionFilter filter)
        {
            var data = queryService.GetAll(filter ?? new TransactionFilter(), UserId);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [Route("{transactionId:Guid}")]
        [HttpDelete]
        public HttpResponseMessage Remove(Guid transactionId)
        {
            var command = new RemoveTransactionCommand(transactionId);
            dispatcher.Dispatch(command);
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        [Route("{transactionId:Guid}")]
        [HttpPut]
        public HttpResponseMessage Update(Transaction model, Guid transactionId)
        {
            var command = new UpdateTransactionCommand()
            {
                Id = transactionId,
                Amount = model.Amount,
                CategoryId = model.CategoryId,
                UserId = UserId
            };

            dispatcher.Dispatch(command);
            return Request.CreateResponse(HttpStatusCode.OK, queryService.GetById(transactionId));
        }
    }
}