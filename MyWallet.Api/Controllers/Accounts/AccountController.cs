﻿using MyWallet.Api.Model;
using MyWallet.Infrastructure;
using MyWallet.Service.Accounts;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyWallet.Controllers.Accounts
{
    [Authorize]
    [RoutePrefix("api/accounts")]
    public class AccountController : BaseController
    {
        private readonly AccountQueryService queryService;
        private readonly IDispatcher dispatcher;

        public AccountController(
            AccountQueryService queryService,
            IDispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
            this.queryService = queryService;
        }

        [Route]
        [HttpGet]
        public HttpResponseMessage GetAccounts()
        {
            return Request.CreateResponse(HttpStatusCode.OK, queryService.GetAccounts(UserId));
        }

        [Route("{accountId:Guid}")]
        [HttpGet]
        public HttpResponseMessage GetAccount(Guid accountId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, queryService.GetById(accountId));
        }

        [Route]
        [HttpPost]
        public HttpResponseMessage CreateAccount(Account model)
        {
            var id = Guid.NewGuid();
            var accountCommand = new CreateAccountCommand()
            {
                Name = model.Name,
                Id = id,
                UserId = UserId
            };

            dispatcher.Dispatch(accountCommand);

            return Request.CreateResponse(HttpStatusCode.OK, queryService.GetById(id));
        }
        
        [Route("{accountId:Guid}")]
        [HttpPut]
        public HttpResponseMessage UpdateAccount(Account model, Guid accountId)
        {
            var accountCommand = new UpdateAccountCommand()
            {
                Id = accountId,
                Name = model.Name,
                UserId = UserId
            };

            dispatcher.Dispatch(accountCommand);

            return Request.CreateResponse(HttpStatusCode.OK, queryService.GetById(accountId));
        }

        [Route("{accountId:Guid}")]
        [HttpDelete]
        public HttpResponseMessage RemoveAccount(Guid accountId)
        {
            var removeAccountCommand = new RemoveAccountCommand(accountId);
            dispatcher.Dispatch(removeAccountCommand);
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}