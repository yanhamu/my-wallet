﻿using MyWallet.Api.Model;
using MyWallet.Infrastructure;
using MyWallet.Service.Accounts.Fio;
using MyWallet.Service.AccountSynchronization;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyWallet.Controllers.Accounts
{
    [Authorize]
    [RoutePrefix("api/accounts/fio")]
    public class FioAccountController : BaseController
    {
        private readonly IDispatcher dispatcher;
        private readonly FioAccountQueryService queryService;

        public FioAccountController(
            IDispatcher dispatcher,
            FioAccountQueryService queryService)
        {
            this.dispatcher = dispatcher;
            this.queryService = queryService;
        }

        [Route]
        [HttpPost]
        public HttpResponseMessage CreateFioAccount(FioAccount fioModel)
        {
            var id = Guid.NewGuid();
            var accountModel = new CreateFioAccountCommand(
                id,
                fioModel.Name,
                fioModel.Token,
                UserId);

            dispatcher.Dispatch(accountModel);

            return Request.CreateResponse(HttpStatusCode.OK, queryService.GetById(id));
        }

        [Route("{id:Guid}/sync")]
        [HttpPost]
        public HttpResponseMessage Synchronize(Guid id)
        {
            var from = queryService.GetById(id).LastSyncDate ?? DateTime.UtcNow.AddMonths(-2);
            var to = DateTime.UtcNow;

            var command = new SyncFioCommand(id, from, to);
            dispatcher.Dispatch(command);

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}