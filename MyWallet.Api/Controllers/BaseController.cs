﻿using System;
using System.Security.Claims;
using System.Web.Http;

namespace MyWallet.Controllers
{
    public class BaseController : ApiController
    {
        public Guid UserId
        {
            get
            {
                return Guid.Parse(((ClaimsPrincipal)this.User).FindFirst("userId").Value);
            }
        }
    }
}