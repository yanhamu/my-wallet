﻿using MyWallet.Api.Model;
using MyWallet.Filters;
using MyWallet.Infrastructure;
using MyWallet.Service.Categories;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyWallet.Controllers.Categories
{
    [Authorize]
    [RoutePrefix("api/categories")]
    public class CategoryController : BaseController
    {
        private readonly CategoryQueryService queryService;
        private readonly IDispatcher dispatcher;

        public CategoryController(
            CategoryQueryService queryService,
            IDispatcher dispatcher)
        {
            this.queryService = queryService;
            this.dispatcher = dispatcher;
        }

        [Route]
        [HttpGet]
        public HttpResponseMessage GetCategories(bool flat = false)
        {
            return Request.CreateResponse(
                HttpStatusCode.OK,
                flat
                    ? queryService.GetFlat(UserId)
                    : queryService.GetHierarchy(UserId));
        }

        [Route("{categoryId:Guid}")]
        [HttpGet]
        public HttpResponseMessage GetCategory(Guid categoryId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, queryService.GetById(categoryId));
        }

        [Route]
        [HttpPost]
        public HttpResponseMessage CreateCategory(Category model)
        {
            var id = Guid.NewGuid();

            var command = new CreateCategoryCommand()
            {
                Id = id,
                Name = model.Name,
                ParentId = model.ParentId,
                UserId = UserId,
                Color = model.Color
            };

            dispatcher.Dispatch(command);

            return Request.CreateResponse(HttpStatusCode.OK, queryService.GetById(id));
        }

        [Route("{categoryId:Guid}")]
        [HttpPut]
        public HttpResponseMessage UpdateCategory(Category model, Guid categoryId)
        {
            var command = new UpdateCategoryCommand()
            {
                Name = model.Name,
                Color = model.Color,
                ParentId = model.ParentId,
                CategoryId = categoryId
            };

            dispatcher.Dispatch(command);

            return Request.CreateResponse(HttpStatusCode.OK, queryService.GetById(categoryId));
        }

        [Route("{categoryId:Guid}")]
        [HttpDelete]
        public HttpResponseMessage RemoveCategory(Guid categoryId)
        {
            var command = new RemoveCategoryCommand();
            command.CategoryId = categoryId;
            dispatcher.Dispatch(command);

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}