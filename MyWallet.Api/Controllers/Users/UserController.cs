﻿using MyWallet.Filters.Exceptions;
using MyWallet.Infrastructure;
using MyWallet.Service.Authentication;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyWallet.Controllers.Users
{
    [RoutePrefix("api/users")]
    public class UserController : BaseController
    {
        private readonly IUserQueryService queryService;
        private readonly IDispatcher dispatcher;

        public UserController(
            IDispatcher dispatcher,
            IUserQueryService queryService)
        {
            this.dispatcher = dispatcher;
            this.queryService = queryService;
        }

        [Authorize]
        [Route]
        [HttpGet]
        public HttpResponseMessage GetUser()
        {
            var user = queryService.GetUser(UserId);
            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        [Route]
        [HttpPost]
        [ExceptionFilter(typeof(DbUpdateException), HttpStatusCode.Conflict, "user with same username already exists")]
        public HttpResponseMessage CreateUser(CreateUserModel model)
        {
            dispatcher.Dispatch(new CreateUserDefaultAuthenticationCommand(model.Username, model.Password));
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }

    public class CreateUserModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }

        public CreateUserModel() { }
        public CreateUserModel(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }
    }
}
