@echo off

REM http://www.allenconway.net/2015/06/using-opencover-and-reportgenerator-to.html

@echo off
SET project=%CD%\
SET openCoverPath=packages\OpenCover.4.6.519\tools\OpenCover.Console.exe
SET xunitPath=packages\xunit.runner.console.2.1.0\tools\xunit.console.exe
SET testPath1=MyWallet.Infrastructure.Tests\bin\Debug\MyWallet.Infrastructure.Tests.dll
SET testPath2=MyWallet.Api.Tests\bin\Debug\MyWallet.Api.Tests.dll
SET outputPath=%project%TestResults\
SET reportGeneratorPath=packages\ReportGenerator.2.5.2\tools\ReportGenerator.exe

SET openCoverFullPath=%project%%openCoverPath%
SET xunitFullPath=%project%%xunitPath%
SET testFullPath=%project%%testPath1% %project%%testPath2%
SET reportGeneratorFullPath=%project%%reportGeneratorPath%
SET openCoverResultFullPath=%outputPath%open-cover-result.xml

@echo on
%openCoverFullPath% -register:user -target:%xunitFullPath% -targetargs:"%testFullPath% -noshadow" -output:%openCoverResultFullPath% -nodefaultfilters -filter:"+[MyWallet.*]* -[*Tests]*"

%reportGeneratorFullPath% -reports:%openCoverResultFullPath% -targetdir:%outputPath%

start %outputPath%index.htm