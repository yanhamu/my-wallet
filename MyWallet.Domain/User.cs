﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MyWallet.Domain
{
    public class User
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserDefaultAuthentication DefaultAuthentication { get; set; }

        public static User CreateViaDefaultAuth(string username, byte[] hashedPassword, byte[] salt, Guid id)
        {
            var defaultAuthentication = new UserDefaultAuthentication();
            defaultAuthentication.Username = username;
            defaultAuthentication.HashedPassword = hashedPassword;
            defaultAuthentication.Salt = salt;
            defaultAuthentication.UserId = id;

            return new User()
            {
                Id = id,
                DefaultAuthentication = defaultAuthentication
            };
        }
    }

    public class UserMapping : EntityTypeConfiguration<User>
    {
        public UserMapping()
        {
            this.HasKey(k => k.Id);
            this.Property(k => k.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.HasOptional(u => u.DefaultAuthentication)
                .WithRequired(d => d.User);

        }
    }
}
