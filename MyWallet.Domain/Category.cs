﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MyWallet.Domain
{
    public class Category
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public Category ParentCategory { get; set; }
        public virtual ICollection<Category> Children { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public bool IsDefault { get; set; }
    }

    public class CategoryMapping : EntityTypeConfiguration<Category>
    {
        public CategoryMapping()
        {
            this.HasKey(k => k.Id);
            this.Property(k => k.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.HasOptional(p => p.ParentCategory)
                .WithMany(c => c.Children)
                .HasForeignKey(k => k.ParentId)
                .WillCascadeOnDelete(false);
            this.HasRequired(p => p.User)
                .WithMany()
                .HasForeignKey(p => p.UserId)
                .WillCascadeOnDelete(true);
            this.Property(c => c.Name).IsRequired();
        }
    }
}