﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace MyWallet.Domain
{
    public class UserDefaultAuthentication
    {
        public Guid UserId { get; set; }
        public User User { get; set; }
        public byte[] HashedPassword { get; set; }
        public byte[] Salt { get; set; }
        public string Username { get; set; }
    }

    public class UserDefaultAuthenticationMapping : EntityTypeConfiguration<UserDefaultAuthentication>
    {
        public UserDefaultAuthenticationMapping()
        {
            this.HasKey(k => k.UserId);
            this.Property(p => p.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.HasRequired(r => r.User)
                .WithOptional(r => r.DefaultAuthentication);
            this.Property(p => p.Username)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute[] { new IndexAttribute() { IsUnique = true } }
                    ))
                .HasMaxLength(40);
        }
    }
}
