﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace MyWallet.Domain
{
    public class FioAccount : Account
    {
        public string Token { get; set; }
        public DateTime? LastSyncDate { get; set; }
    }

    public class FioAccountMapping : EntityTypeConfiguration<FioAccount>
    {
        public FioAccountMapping()
        {
            this.ToTable("FioAccount");
            this.Property(p => p.Token).IsRequired();
        }
    }
}