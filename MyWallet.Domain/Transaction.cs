﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MyWallet.Domain
{
    public class Transaction
    {
        public Guid Id { get; set; }
        public Guid AccountId { get; set; }
        public Account Account { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public DateTime MovementDate { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
        public bool WasSpotted { get; set; }

        public Transaction() { }
    }

    public class TransactionMapping : EntityTypeConfiguration<Transaction>
    {
        public TransactionMapping()
        {
            this.HasKey(k => k.Id);
            this.Property(k => k.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.HasRequired(t => t.Account)
                .WithMany()
                .HasForeignKey(k => k.AccountId)
                .WillCascadeOnDelete(true);
            this.HasRequired(t => t.Category)
                .WithMany()
                .HasForeignKey(t => t.CategoryId)
                .WillCascadeOnDelete(false);
        }
    }
}
