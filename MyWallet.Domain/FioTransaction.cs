﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MyWallet.Domain
{
    public class FioTransaction : Transaction
    {
        /// <summary>
        /// ID pohybu
        /// </summary>
        public long MovementId { get; set; }

        /// <summary>
        /// Měna
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Protiúčet
        /// </summary>
        public string ContraAccount { get; set; }
        /// <summary>
        /// Název protiúčtu
        /// </summary>
        public string ContraAccountName { get; set; }
        /// <summary>
        /// Kód banky
        /// </summary>
        public string ContraAccountBankCode { get; set; }
        /// <summary>
        /// Název banky
        /// </summary>
        public string ContraAccountBankName { get; set; }
        /// <summary>
        /// KS
        /// </summary>
        public int? ConstantSymbol { get; set; }
        /// <summary>
        /// VS
        /// </summary>
        public long? VariableSymbol { get; set; }
        /// <summary>
        /// SS
        /// </summary>
        public long? SpecificSymbol { get; set; }
        /// <summary>
        /// Uživatelská identifikace
        /// </summary>
        public string UserIdentification { get; set; }
        /// <summary>
        /// Zpráva pro příjemce
        /// </summary>
        public string MessageForReceiver { get; set; }
        /// <summary>
        /// Typ
        /// </summary>
        public string TransactionType { get; set; }
        /// <summary>
        /// Provedl
        /// </summary>
        public string TransactionExecutive { get; set; }
        /// <summary>
        /// Upřesnění
        /// </summary>
        public string Specification { get; set; }
        /// <summary>
        /// Komentář
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// BIC
        /// </summary>
        public string BankIndentifierCode { get; set; }
        /// <summary>
        /// ID Pokynu
        /// </summary>
        public long? TransactionId { get; set; }
    }

    public class FioTransactionMapping : EntityTypeConfiguration<FioTransaction>
    {
        public FioTransactionMapping()
        {
            this.ToTable("FioTransaction");
            this.HasRequired(t => t.Account)
                .WithMany()
                .HasForeignKey(k => k.AccountId)
                .WillCascadeOnDelete(true);

            this.Property(t => t.MovementId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
