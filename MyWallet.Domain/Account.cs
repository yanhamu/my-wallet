﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MyWallet.Domain
{
    public class Account
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public string Name { get; set; }
    }

    public class AccountMapping : EntityTypeConfiguration<Account>
    {
        public AccountMapping()
        {
            this.HasKey(k => k.Id);
            this.Property(p => p.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.HasRequired(p => p.User)
                .WithMany()
                .HasForeignKey(k => k.UserId);
        }
    }
}
