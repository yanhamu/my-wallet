﻿using System;
using System.Collections.Generic;

namespace Common.Library.UrlHelper
{
    public class UrlBuilder
    {
        private List<IUrlPart> urlParts = new List<IUrlPart>();

        public UrlBuilder(string baseUrl)
        {
            urlParts.Add(new UrlSegment(baseUrl));
        }

        public string GetUrl()
        {
            var url = string.Empty;
            foreach (var part in urlParts)
            {
                switch (part)
                {
                    case UrlSegment s:
                        url += "/" + s.Segment;
                        break;
                    default:
                        return url.TrimStart('/');
                }
            }
            return url.TrimStart('/');
        }

        public string GetUrl(params Guid[] ids)
        {
            var url = string.Empty;
            var valuesIndex = 0;

            foreach (var part in urlParts)
            {
                switch (part)
                {
                    case UrlSegment s:
                        url += "/" + s.Segment;
                        break;
                    case UrlPlaceholder p:
                        if (ShouldStop(valuesIndex, ids.Length))
                            return url.TrimStart('/');

                        url += "/" + ids[valuesIndex].ToString();
                        valuesIndex += 1;
                        break;
                    default:
                        throw new InvalidOperationException("unexpected url type in parts");
                }
            }
            return url.TrimStart('/');
        }

        private bool ShouldStop(int index, int length)
        {
            return length <= index;
        }

        public UrlBuilder AddUrlSegment(string segment)
        {
            urlParts.Add(new UrlSegment(segment));
            return this;
        }

        public UrlBuilder AddPlaceholder()
        {
            urlParts.Add(new UrlPlaceholder());
            return this;
        }
    }
}
