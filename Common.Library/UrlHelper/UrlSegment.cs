﻿namespace Common.Library.UrlHelper
{
    public class UrlSegment : IUrlPart
    {
        public string Segment { get; private set; }

        public UrlSegment(string urlSegment)
        {
            this.Segment = urlSegment;
        }

        public override string ToString()
        {
            return Segment;
        }
    }
}
