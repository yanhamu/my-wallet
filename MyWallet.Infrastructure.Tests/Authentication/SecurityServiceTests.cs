﻿using MyWallet.Infrastructure.Authentication;
using Xunit;

namespace MyWallet.Infrastructure.Tests.Authentication
{
    public class SecurityServiceTests
    {
        [Fact]
        public void Should_SuccessfullyVerify()
        {
            var password = "Genymede6";
            var securityService = new SecurityService();
            var salt = securityService.GetSalt();
            var hashed = securityService.CreateHash(password, salt);
            
            Assert.True(securityService.VerifyPassword(password, hashed, salt));
        }
    }
}