﻿using RestSharp;
using System;
using System.Threading.Tasks;

namespace MyWallet.Fio.Domain
{
    public class FioApiClient : IFioApiClient
    {
        private readonly string baseUrl;

        public FioApiClient(string fioBaseUrl)
        {
            this.baseUrl = fioBaseUrl;
        }

        public Task<IRestResponse<AccountStatement>> GetAccountStatementAsync(string token, DateTime from, DateTime to)
        {
            var client = new RestClient(baseUrl);
            var request = new RestRequest("periods/{token}/{from}/{to}/transactions.xml");
            request.AddUrlSegment("token", token);
            request.AddUrlSegment("from", from.ToString("yyyy-MM-dd"));
            request.AddUrlSegment("to", to.ToString("yyyy-MM-dd"));

            return client.ExecuteTaskAsync<AccountStatement>(request);
        }
    }
}
