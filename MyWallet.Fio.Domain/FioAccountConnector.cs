﻿using MyWallet.Fio.Domain;
using Polly;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace MyWallet.Fio.DataAccess
{
    public class FioAccountConnector
    {
        private readonly IFioApiClient fioClient;

        public FioAccountConnector(IFioApiClient fioClient)
        {
            this.fioClient = fioClient;
        }

        public async Task<AccountStatement> RetrievePayments(string token, DateTime from, DateTime to)
        {
            var policy = Policy.HandleResult<IRestResponse<AccountStatement>>(r => r.StatusCode != HttpStatusCode.OK)
                .WaitAndRetry(3, retryAttempt => TimeSpan.FromSeconds(1));

            var result = await policy.ExecuteAsync(() => fioClient.GetAccountStatementAsync(token, from, to));
            if (result.StatusCode != HttpStatusCode.OK || result.Data == null)
                throw new HttpResponseException(result.StatusCode, "received unexpected response");

            return result.Data;
        }
    }
}