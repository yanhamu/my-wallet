﻿using RestSharp;
using System;
using System.Threading.Tasks;

namespace MyWallet.Fio.Domain
{
    public interface IFioApiClient
    {
        Task<IRestResponse<AccountStatement>> GetAccountStatementAsync(string token, DateTime from, DateTime to);
    }
}