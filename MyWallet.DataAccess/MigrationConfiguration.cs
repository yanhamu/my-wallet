﻿using System.Data.Entity.Migrations;

namespace MyWallet.DataAccess
{
    public class MigrationConfiguration : DbMigrationsConfiguration<MyWalletDbContext>
    {
        public MigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
    }
}