﻿using System.Data.Entity;

namespace MyWallet.DataAccess
{
    public static class DatabaseInitializer
    {
        public static void Initialize()
        {
            Database.SetInitializer(new DatabaseInitializerStrategy());
        }
    }
}
