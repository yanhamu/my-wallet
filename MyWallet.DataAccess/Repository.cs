﻿using System.Data.Entity;

namespace MyWallet.DataAccess
{
    public class Repository<T> where T : class
    {
        protected readonly MyWalletDbContext context;
        public DbSet<T> Set { get; private set; }

        public Repository(MyWalletDbContext context)
        {
            this.context = context;
            this.Set = context.Set<T>();
        }

        public T Find(params object[] keyValues)
        {
            return Set.Find(keyValues);
        }

        public T Add(T entity)
        {
            return Set.Add(entity);
        }

        public T Delete(T entity)
        {
            return Set.Remove(entity);
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }
    }
}