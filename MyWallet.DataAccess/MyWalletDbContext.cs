﻿using MyWallet.Domain;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;

namespace MyWallet.DataAccess
{
    public class MyWalletDbContext : DbContext
    {
        public MyWalletDbContext() : base("MyWallet")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserDefaultAuthentication> UserDefaultAuthentication { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<FioAccount> FioAccounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<FioTransaction> FioTransaction { get; set; }
        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            Assembly.GetAssembly(typeof(CategoryMapping))
                .GetTypes()
                .Where(t => t.BaseType.IsGenericType)
                .Where(t => t.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>))
                .Select(t => t)
                .ToList()
                .ForEach(t =>
                {
                    dynamic instance = Activator.CreateInstance(t);
                    modelBuilder.Configurations.Add(instance);
                });

            base.OnModelCreating(modelBuilder);
        }
    }
}