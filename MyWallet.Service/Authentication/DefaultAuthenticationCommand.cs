﻿namespace MyWallet.Service.Authentication
{
    public class DefaultAuthenticationCommand
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
