﻿using MyWallet.Api.Model;
using System;

namespace MyWallet.Service.Authentication
{
    public interface IUserQueryService
    {
        User GetUser(Guid userId);
    }
}