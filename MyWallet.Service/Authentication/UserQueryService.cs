﻿using AutoMapper;
using MyWallet.Api.Model;
using MyWallet.DataAccess;
using System;

namespace MyWallet.Service.Authentication
{
    public class UserQueryService : IUserQueryService
    {
        private readonly Repository<Domain.User> userRepository;
        public IMapper Mapper { get; set; }

        public UserQueryService(Repository<Domain.User> userRepository)
        {
            this.userRepository = userRepository;
        }

        public User GetUser(Guid userId)
        {
            return Mapper.Map<User>(userRepository.Set.Find(userId));
        }
    }
}
