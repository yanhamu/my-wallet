﻿using MyWallet.Infrastructure.Commands;
using System;

namespace MyWallet.Service.Authentication
{
    public class CreateUserDefaultAuthenticationCommand : ICommand
    {
        public CreateUserDefaultAuthenticationCommand(string username, string password)
        {
            this.Id = Guid.NewGuid();
            this.Username = username;
            this.Password = password;
        }

        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}