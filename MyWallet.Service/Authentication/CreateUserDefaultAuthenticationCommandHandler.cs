﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Authentication;
using MyWallet.Infrastructure.Commands;
using MyWallet.Service.Categories;
using System;

namespace MyWallet.Service.Authentication
{
    public class CreateUserDefaultAuthenticationCommandHandler : ICommandHandler<CreateUserDefaultAuthenticationCommand>
    {
        private readonly ICommandHandler<CreateCategoryCommand> handler;
        private readonly SecurityService securityService;
        private readonly Repository<User> userRepository;

        public CreateUserDefaultAuthenticationCommandHandler(
            Repository<User> userRepository,
            SecurityService securityService,
            ICommandHandler<CreateCategoryCommand> handler)
        {
            this.userRepository = userRepository;
            this.securityService = securityService;
            this.handler = handler;
        }

        public void Handle(CreateUserDefaultAuthenticationCommand command)
        {
            var salt = securityService.GetSalt();
            var hashedPassword = securityService.CreateHash(command.Password, salt);
            var user = userRepository.Add(User.CreateViaDefaultAuth(command.Username, hashedPassword, salt, command.Id));

            userRepository.SaveChanges();

            var createCategoryCommand = new CreateCategoryCommand() { Id = Guid.NewGuid(), Name = "Not recognized", UserId = command.Id, Color = "8b8b8b", IsDefault = true };
            handler.Handle(createCategoryCommand);
        }
    }
}
