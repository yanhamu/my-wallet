﻿using MyWallet.Infrastructure.Commands;
using System;

namespace MyWallet.Service.AccountSynchronization
{
    public class SyncFioCommand : ICommand
    {
        public Guid AccountId { get; private set; }

        public DateTime From { get; private set; }
        public DateTime To { get; private set; }

        public SyncFioCommand(
            Guid accountId,
            DateTime from,
            DateTime to)
        {
            this.AccountId = accountId;
            this.From = from;
            this.To = to;
        }
    }
}
