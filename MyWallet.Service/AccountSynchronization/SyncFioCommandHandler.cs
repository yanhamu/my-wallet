﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Fio.DataAccess;
using MyWallet.Infrastructure.Commands;
using MyWallet.Service.CategoryProviders;
using System;

namespace MyWallet.Service.AccountSynchronization
{
    public class SyncFioCommandHandler : ICommandHandler<SyncFioCommand>
    {
        private readonly FioAdapter adapter;
        private readonly ICategoryProvider categoryProvider;
        private readonly FioAccountConnector fioConnector;
        private readonly Repository<FioAccount> repository;
        private readonly Repository<FioTransaction> transactionRepository;

        public SyncFioCommandHandler(
            Repository<FioAccount> fioAccountRepository,
            FioAccountConnector fioConnector,
            FioAdapter adapter,
            ICategoryProvider categoryProvider,
            Repository<FioTransaction> transactionRepository)
        {
            this.repository = fioAccountRepository;
            this.transactionRepository = transactionRepository;
            this.fioConnector = fioConnector;
            this.categoryProvider = categoryProvider;
            this.adapter = adapter;
        }

        public async void Handle(SyncFioCommand command)
        {
            var account = repository.Find(command.AccountId);
            var accountStatement = await fioConnector.RetrievePayments(account.Token, command.From, command.To);

            var defaultCategory = categoryProvider.GetCategory(account.UserId);

            foreach (var transaction in accountStatement.TransactionList)
            {
                var fioTransaction = adapter.Transform(transaction, command.AccountId, defaultCategory.Id, Guid.NewGuid());
                transactionRepository.Add(fioTransaction);
            }

            account.LastSyncDate = command.To;
            repository.SaveChanges();
        }
    }
}