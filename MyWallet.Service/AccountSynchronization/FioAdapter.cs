﻿using MyWallet.Domain;
using System;

namespace MyWallet.Service.AccountSynchronization
{
    public class FioAdapter
    {
        public FioTransaction Transform(AccountStatementTransaction rawTransaction, Guid accountId, Guid categoryId, Guid transactionId)
        {
            var result = new FioTransaction();

            result.Id = transactionId;
            result.CategoryId = categoryId;
            result.AccountId = accountId;
            result.MovementId = long.Parse(rawTransaction.column_22.Value);
            result.MovementDate = rawTransaction.column_0.Value;
            result.Amount = rawTransaction.column_1.Value;
            result.Currency = rawTransaction.column_14.Value;
            result.ContraAccount = rawTransaction.column_2 == null ? null : rawTransaction.column_2.Value;
            result.ContraAccountName = rawTransaction.column_10 == null ? null : rawTransaction.column_10.Value;
            result.ContraAccountBankCode = rawTransaction.column_3 == null ? null : rawTransaction.column_3.Value;
            result.ContraAccountBankName = rawTransaction.column_12 == null ? null : rawTransaction.column_12.Value;
            result.ConstantSymbol = rawTransaction.column_4 == null ? default(int?) : int.Parse(rawTransaction.column_4.Value);
            result.VariableSymbol = rawTransaction.column_5 == null ? default(long?) : long.Parse(rawTransaction.column_5.Value);
            result.SpecificSymbol = rawTransaction.column_6 == null ? default(long?) : long.Parse(rawTransaction.column_6.Value);
            result.UserIdentification = rawTransaction.column_7 == null ? null : rawTransaction.column_7.Value;
            result.MessageForReceiver = rawTransaction.column_16 == null ? null : rawTransaction.column_16.Value;
            result.TransactionType = rawTransaction.column_8 == null ? null : rawTransaction.column_8.Value;
            result.TransactionExecutive = rawTransaction.column_9 == null ? null : rawTransaction.column_9.Value;
            result.Specification = rawTransaction.column_18 == null ? null : rawTransaction.column_18.Value;
            result.Comment = rawTransaction.column_25 == null ? null : rawTransaction.column_25.Value;
            result.BankIndentifierCode = rawTransaction.column_26 == null ? null : rawTransaction.column_26.Value;
            result.TransactionId = rawTransaction.column_17 == null ? default(long?) : rawTransaction.column_17.Value;

            result.Description = result.MessageForReceiver;

            return result;
        }
    }
}
