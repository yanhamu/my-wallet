﻿using AutoMapper;
using MyWallet.Api.Model;
using MyWallet.DataAccess;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MyWallet.Service.Categories
{
    public class CategoryQueryService
    {
        private readonly Repository<Domain.Category> repository;
        public IMapper Mapper { get; set; }

        public CategoryQueryService(Repository<Domain.Category> repository)
        {
            this.repository = repository;
        }

        public Category GetById(Guid categoryId)
        {
            return Mapper.Map<Category>(repository.Find(categoryId));
        }

        /// <summary>
        /// Gets category hierarchy with children loaded
        /// </summary>
        /// <param name="userId">owner of category hierarchy</param>
        /// <returns>Collection of category root with whole hierarchy loaded</returns>
        public IEnumerable<HierarchicalCategory> GetHierarchy(Guid userId)
        {
            var categories = repository
                .Set
                .Where(c => c.UserId == userId)
                .Include(c => c.Children)
                .ToList()
                .Where(c => c.ParentId.HasValue == false);
            return Mapper.Map<IEnumerable<HierarchicalCategory>>(categories);
        }

        public IEnumerable<Category> GetFlat(Guid userId)
        {
            var categories = repository
                .Set
                .Where(c => c.UserId == userId)
                .ToList();
            return Mapper.Map<IEnumerable<Category>>(categories);
        }
    }
}
