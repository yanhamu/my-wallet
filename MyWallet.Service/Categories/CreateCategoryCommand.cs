﻿using MyWallet.Infrastructure.Commands;
using MyWallet.Service.Handlers;
using System;

namespace MyWallet.Service.Categories
{
    public class CreateCategoryCommand : ICommand
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public bool IsDefault { get; set; }
    }
}
