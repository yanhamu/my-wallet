﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Commands;

namespace MyWallet.Service.Categories
{
    public class UpdateCategoryCommandHandler : ICommandHandler<UpdateCategoryCommand>
    {
        private readonly Repository<Category> repository;

        public UpdateCategoryCommandHandler(Repository<Category> repository)
        {
            this.repository = repository;
        }

        public void Handle(UpdateCategoryCommand command)
        {
            var toUpdate = repository.Find(command.CategoryId);
            toUpdate.Name = command.Name;
            toUpdate.ParentId = command.ParentId;
            toUpdate.Color = command.Color;
            repository.SaveChanges();
        }
    }
}
