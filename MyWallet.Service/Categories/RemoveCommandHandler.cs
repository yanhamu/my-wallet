﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Commands;

namespace MyWallet.Service.Categories
{
    public class RemoveCommandHandler : ICommandHandler<RemoveCategoryCommand>
    {
        private readonly Repository<Category> repository;

        public RemoveCommandHandler(Repository<Category> repository)
        {
            this.repository = repository;
        }

        public void Handle(RemoveCategoryCommand command)
        {
            var toRemove = repository.Find(command.CategoryId);
            repository.Delete(toRemove);
            repository.SaveChanges();
        }
    }
}
