﻿using MyWallet.Infrastructure.Commands;
using MyWallet.Service.Handlers;
using System;

namespace MyWallet.Service.Categories
{
    public class UpdateCategoryCommand : ICommand
    {
        public Guid CategoryId { get; set; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
    }
}
