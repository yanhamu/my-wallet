﻿using MyWallet.Infrastructure.Commands;
using System;

namespace MyWallet.Service.Categories
{
    public class RemoveCategoryCommand : ICommand
    {
        public Guid CategoryId { get; set; }
    }
}
