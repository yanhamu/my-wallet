﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Commands;

namespace MyWallet.Service.Categories
{
    public class CreateCategoryCommandHandler : ICommandHandler<CreateCategoryCommand>
    {
        private readonly Repository<Category> repository;

        public CreateCategoryCommandHandler(Repository<Category> repository)
        {
            this.repository = repository;
        }

        public void Handle(CreateCategoryCommand command)
        {
            var category = new Category()
            {
                Id = command.Id,
                ParentId = command.ParentId,
                UserId = command.UserId,
                Name = command.Name,
                Color = command.Color,
                IsDefault = command.IsDefault
            };
            repository.Set.Add(category);
            repository.SaveChanges();
        }
    }
}
