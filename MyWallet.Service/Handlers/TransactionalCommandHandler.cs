﻿using MyWallet.DataAccess;
using MyWallet.Infrastructure.Commands;

namespace MyWallet.Service.Handlers
{
    public class TransactionalCommandHandler<TCommand> : ICommandHandler<TCommand> where TCommand : ICommand
    {
        private readonly MyWalletDbContext context;
        private readonly ICommandHandler<TCommand> decorated;

        public TransactionalCommandHandler(ICommandHandler<TCommand> decorated, MyWalletDbContext context)
        {
            this.decorated = decorated;
            this.context = context;
        }

        public void Handle(TCommand command)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    decorated.Handle(command);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
