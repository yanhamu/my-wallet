﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using System;
using System.Linq;

namespace MyWallet.Service.CategoryProviders
{
    public class DefaultCategoryProvider : ICategoryProvider
    {
        private readonly Repository<Category> repository;

        public DefaultCategoryProvider(Repository<Category> repository)
        {
            this.repository = repository;
        }

        public Category GetCategory(Guid userId)
        {
            return repository
                .Set
                .Where(c => c.UserId == userId)
                .Where(c => c.IsDefault)
                .Single();
        }
    }
}
