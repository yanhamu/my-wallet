﻿using MyWallet.Domain;
using System;

namespace MyWallet.Service.CategoryProviders
{
    public interface ICategoryProvider
    {
        Category GetCategory(Guid userId);
    }
}