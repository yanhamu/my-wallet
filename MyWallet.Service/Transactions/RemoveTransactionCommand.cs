﻿using MyWallet.Infrastructure.Commands;
using System;

namespace MyWallet.Service.Transactions
{
    public class RemoveTransactionCommand : ICommand
    {
        public Guid TransactionId { get; private set; }
        public RemoveTransactionCommand(Guid transactionId)
        {
            TransactionId = transactionId;
        }

    }
}
