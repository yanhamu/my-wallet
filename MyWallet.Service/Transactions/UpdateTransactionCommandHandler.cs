﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Commands;
using MyWallet.Service.CategoryProviders;
using System;

namespace MyWallet.Service.Transactions
{
    public class UpdateTransactionCommandHandler : ICommandHandler<UpdateTransactionCommand>
    {
        private readonly ICategoryProvider categoryProvider;
        private readonly Repository<Transaction> repository;

        public UpdateTransactionCommandHandler(
            Repository<Transaction> repository,
            ICategoryProvider categoryProvider)
        {
            this.repository = repository;
            this.categoryProvider = categoryProvider;
        }

        public void Handle(UpdateTransactionCommand command)
        {
            var transaction = repository.Find(command.Id);
            transaction.Amount = command.Amount;
            transaction.CategoryId = GetCategoryId(command.UserId, command.CategoryId);
            repository.SaveChanges();
        }

        private Guid GetCategoryId(Guid userId, Guid? categoryGuid)
        {
            if (categoryGuid.HasValue)
                return categoryGuid.Value;
            return categoryProvider.GetCategory(userId).Id;
        }
    }
}
