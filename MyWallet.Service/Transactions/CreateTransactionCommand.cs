﻿using MyWallet.Infrastructure.Commands;
using System;

namespace MyWallet.Service.Transactions
{
    public class CreateTransactionCommand : ICommand
    {
        public Guid Id { get; set; }
        public Guid AccountId { get; set; }
        public decimal Amount { get; set; }
        public DateTime MovementDate { get; set; }
        public Guid? CategoryId { get; set; }
        public Guid UserId { get; set; }
        public bool WasSpotted { get; set; }
    }
}