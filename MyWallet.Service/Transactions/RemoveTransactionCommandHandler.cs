﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Commands;

namespace MyWallet.Service.Transactions
{
    public class RemoveTransactionCommandHandler : ICommandHandler<RemoveTransactionCommand>
    {
        private readonly Repository<Transaction> repository;

        public RemoveTransactionCommandHandler(Repository<Transaction> repository)
        {
            this.repository = repository;
        }

        public void Handle(RemoveTransactionCommand command)
        {
            var transaction = this.repository.Find(command.TransactionId);
            repository.Delete(transaction);
            repository.SaveChanges();
        }
    }
}
