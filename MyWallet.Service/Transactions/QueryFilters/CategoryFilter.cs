﻿using MyWallet.Api.Model.Filters;
using MyWallet.Api.Model.QueryBuilders;
using MyWallet.Domain;
using System.Linq;

namespace MyWallet.Service.Transactions.QueryFilters
{
    public class CategoryFilter : IQueryBuilder<TransactionFilter, Transaction>
    {
        public IQueryable<Transaction> Filter(TransactionFilter filter, IQueryable<Transaction> query)
        {
            if (filter.CategoryId.HasValue)
                query = query.Where(q => q.CategoryId == filter.CategoryId.Value);
            return query;
        }
    }
}
