﻿using MyWallet.Api.Model.Filters;
using MyWallet.Api.Model.QueryBuilders;
using MyWallet.Domain;
using System.Linq;

namespace MyWallet.Service.Transactions.QueryFilters
{
    public class SpottedFilter : IQueryBuilder<TransactionFilter, Transaction>
    {
        public IQueryable<Transaction> Filter(TransactionFilter filter, IQueryable<Transaction> query)
        {
            if (filter.Spotted.HasValue)
                query = query.Where(q => q.WasSpotted == filter.Spotted.Value);
            return query;
        }
    }
}