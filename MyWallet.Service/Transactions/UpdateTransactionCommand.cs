﻿using MyWallet.Infrastructure.Commands;
using MyWallet.Service.Handlers;
using System;

namespace MyWallet.Service.Transactions
{
    public class UpdateTransactionCommand : ICommand
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public decimal Amount { get; set; }
        public Guid? CategoryId { get; set; }
    }
}
