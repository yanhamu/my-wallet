﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Commands;
using MyWallet.Service.CategoryProviders;

namespace MyWallet.Service.Transactions
{
    public class CreateRecordCommandHandler : ICommandHandler<CreateTransactionCommand>
    {
        private readonly ICategoryProvider categoryProvider;
        private readonly Repository<Transaction> repository;

        public CreateRecordCommandHandler(
            Repository<Transaction> repository,
            ICategoryProvider categoryProvider)
        {
            this.repository = repository;
            this.categoryProvider = categoryProvider;
        }

        public void Handle(CreateTransactionCommand command)
        {
            var transaction = new Transaction()
            {
                AccountId = command.AccountId,
                Amount = command.Amount,
                MovementDate = command.MovementDate,
                Id = command.Id,
                CategoryId = command.CategoryId.HasValue
                    ? command.CategoryId.Value
                    : categoryProvider.GetCategory(command.UserId).Id,
                WasSpotted = command.WasSpotted
            };

            repository.Add(transaction);
            repository.SaveChanges();
        }
    }
}