﻿using AutoMapper;
using MyWallet.Api.Model;
using MyWallet.Api.Model.Filters;
using MyWallet.Api.Model.QueryBuilders;
using MyWallet.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyWallet.Service.Transactions
{
    public class TransactionQueryService
    {
        private const int pageSize = 25;
        private readonly Repository<Domain.Transaction> repository;
        private readonly IEnumerable<IQueryBuilder<TransactionFilter, Domain.Transaction>> queryBuilders;

        public IMapper Mapper { get; set; }

        public TransactionQueryService(Repository<Domain.Transaction> repository, IEnumerable<IQueryBuilder<TransactionFilter, Domain.Transaction>> queryBuilders)
        {
            this.repository = repository;
            this.queryBuilders = queryBuilders;
        }

        public Transaction GetById(Guid id)
        {
            return Mapper.Map<Transaction>(repository.Find(id));
        }

        public IEnumerable<TransactionHeader> GetAll(Guid accountId)
        {
            var transactions = repository
                .Set
                .Where(t => t.AccountId == accountId)
                .ToList();
            return Mapper.Map<IEnumerable<TransactionHeader>>(transactions);
        }

        public PagedResponse<TransactionHeader> GetAll(TransactionFilter filter, Guid userId)
        {
            var query = this.repository.Set.Where(t => t.Account.UserId == userId);
            var totalRecords = query.Count();

            var filteredQuery = FilterQuery(filter, query);

            var orderedQuery = filteredQuery.OrderByDescending(t => t.MovementDate);

            var pagedQuery = DoPaging(orderedQuery, filter)
                .Select(t => new TransactionHeader()
                {
                    Id = t.Id,
                    AccountId = t.AccountId,
                    Amount = t.Amount,
                    CategoryId = t.CategoryId
                });

            return new PagedResponse<TransactionHeader>(pagedQuery.ToList(), filter.Page ?? 1, totalRecords, pageSize);
        }

        private IQueryable<Domain.Transaction> FilterQuery(TransactionFilter filter, IQueryable<Domain.Transaction> query)
        {
            foreach (var queryBuilder in queryBuilders)
                query = queryBuilder.Filter(filter, query);

            return query;
        }

        private IQueryable<Domain.Transaction> DoPaging(IQueryable<Domain.Transaction> query, PageFilter filter)
        {
            var pageSize = filter.PageSize ?? TransactionQueryService.pageSize;
            var pageNumber = filter.Page ?? 1;
            return query.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);
        }
    }
}