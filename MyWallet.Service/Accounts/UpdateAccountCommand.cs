﻿using MyWallet.Infrastructure.Commands;
using System;

namespace MyWallet.Service.Accounts
{
    public class UpdateAccountCommand : ICommand
    {
        public Guid UserId { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
