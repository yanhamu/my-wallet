﻿using MyWallet.Infrastructure.Commands;
using System;

namespace MyWallet.Service.Accounts
{
    public class RemoveAccountCommand : ICommand
    {
        public Guid AccountId { get; private set; }

        public RemoveAccountCommand(Guid accountId)
        {
            this.AccountId = accountId;
        }
    }
}
