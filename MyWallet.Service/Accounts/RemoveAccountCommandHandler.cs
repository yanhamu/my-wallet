﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Commands;

namespace MyWallet.Service.Accounts
{
    public class RemoveAccountCommandHandler : ICommandHandler<RemoveAccountCommand>
    {
        private readonly Repository<Account> repository;

        public RemoveAccountCommandHandler(Repository<Account> repository)
        {
            this.repository = repository;
        }
        public void Handle(RemoveAccountCommand command)
        {
            var account = repository.Find(command.AccountId);
            repository.Delete(account);
            repository.SaveChanges();
        }
    }
}
