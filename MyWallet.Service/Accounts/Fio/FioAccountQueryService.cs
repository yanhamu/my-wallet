﻿using AutoMapper;
using MyWallet.Api.Model;
using MyWallet.DataAccess;
using System;

namespace MyWallet.Service.Accounts.Fio
{
    public class FioAccountQueryService
    {
        private readonly Repository<Domain.FioAccount> repository;
        public IMapper Mapper { get; set; }

        public FioAccountQueryService(Repository<Domain.FioAccount> repository)
        {
            this.repository = repository;
        }

        public FioAccount GetById(Guid id)
        {
            return Mapper.Map<FioAccount>(repository.Find(id));
        }
    }
}