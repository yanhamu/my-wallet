﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Commands;

namespace MyWallet.Service.Accounts.Fio
{
    public class CreateFioAccountCommandHandler : ICommandHandler<CreateFioAccountCommand>
    {
        private readonly Repository<FioAccount> repository;

        public CreateFioAccountCommandHandler(Repository<FioAccount> repository)
        {
            this.repository = repository;
        }

        public void Handle(CreateFioAccountCommand command)
        {
            var fioAccount = new FioAccount()
            {
                Id = command.FioAccountId,
                Name = command.Name,
                Token = command.Token,
                UserId = command.UserId
            };

            repository.Add(fioAccount);
            repository.SaveChanges();
        }
    }
}
