﻿using MyWallet.Infrastructure.Commands;
using System;

namespace MyWallet.Service.Accounts.Fio
{
    public class CreateFioAccountCommand : ICommand
    {
        public Guid FioAccountId { get; private set; }
        public string Name { get; private set; }
        public string Token { get; private set; }
        public Guid UserId { get; private set; }

        public CreateFioAccountCommand(
            Guid fioAccountId,
            string name,
            string token,
            Guid userId)
        {
            this.FioAccountId = fioAccountId;
            this.Name = name;
            this.Token = token;
            this.UserId = userId;
        }
    }
}
