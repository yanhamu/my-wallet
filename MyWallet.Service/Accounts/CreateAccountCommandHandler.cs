﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Commands;

namespace MyWallet.Service.Accounts
{
    public class CreateAccountCommandHandler : ICommandHandler<CreateAccountCommand>
    {
        private readonly Repository<Account> repository;

        public CreateAccountCommandHandler(Repository<Account> repository)
        {
            this.repository = repository;
        }

        public void Handle(CreateAccountCommand command)
        {
            var account = new Account()
            {
                Id = command.Id,
                Name = command.Name,
                UserId = command.UserId
            };

            repository.Add(account);
            repository.SaveChanges();
        }
    }
}
