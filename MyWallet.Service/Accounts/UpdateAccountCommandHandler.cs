﻿using MyWallet.DataAccess;
using MyWallet.Domain;
using MyWallet.Infrastructure.Commands;

namespace MyWallet.Service.Accounts
{
    public class UpdateAccountCommandHandler : ICommandHandler<UpdateAccountCommand>
    {
        private readonly Repository<Account> repository;

        public UpdateAccountCommandHandler(Repository<Account> repository)
        {
            this.repository = repository;
        }

        public void Handle(UpdateAccountCommand command)
        {
            var account = repository.Find(command.Id);
            account.Name = command.Name;
            repository.SaveChanges();
        }
    }
}
