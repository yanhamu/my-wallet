﻿using AutoMapper;
using MyWallet.Api.Model;
using MyWallet.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyWallet.Service.Accounts
{
    public class AccountQueryService
    {
        private readonly Repository<Domain.Account> repository;
        public IMapper Mapper { get; set; }

        public AccountQueryService(Repository<Domain.Account> repository)
        {
            this.repository = repository;
        }

        public Account GetById(params object[] keys)
        {
            return Mapper.Map<Account>(repository.Find(keys));
        }

        public IEnumerable<Account> GetAccounts(Guid userId)
        {
            return repository
                .Set
                .Where(a => a.UserId == userId)
                .Select(a => new Account()
                {
                    Id = a.Id,
                    Name = a.Name
                })
                .ToList();
        }
    }
}
