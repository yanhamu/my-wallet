﻿using System;

namespace MyWallet.Api.Tests.Fixtures
{
    public static class CategoryFixtures
    {
        public static Category Default => Category.Create("DA68829D-9B95-4774-92A1-35C8719FE824", UserFixtures.Existing.Guid, "system", true);
        public static Category ToUpdate => Category.Create("57C41F40-89B6-4BAC-8071-7D608F9A9925", UserFixtures.Existing.Guid, "To update");
        public static Category ToDelete => Category.Create("B7459238-A237-41B3-B9B0-7F8A8D554628", UserFixtures.Existing.Guid, "To delete");
        public static Category Foreign => Category.Create("4C49015A-955E-4650-AB61-FEC0EBA3AABC", UserFixtures.Peter.Guid, "Peters");
    }

    public class Category
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public bool IsDefault { get; set; }

        private Category() { }

        public static Category Create(string id, Guid userId, string name, bool isDefault = false)
        {
            return new Category()
            {
                Id = new Guid(id),
                UserId = userId,
                Color = "00ff00",
                IsDefault = isDefault,
                Name = name
            };
        }
    }
}
