﻿using System;

namespace MyWallet.Api.Tests.Fixtures
{
    public static class TransactionFixtures
    {
        public static Transaction Existing = Transaction.Create("cf2df6b8-8076-48f2-8e62-1e54e4140f3e", AccountFixtures.ExistingAccount_Cash.Id, "To get", new DateTime(2017, 1, 30, 9, 0, 0), CategoryFixtures.Default.Id, 20.5m);
        public static Transaction ToUpdate = Transaction.Create("3c9813e6-bbaa-44f8-bf68-3277df301d50", AccountFixtures.ExistingAccount_Cash.Id, "To update / delete", new DateTime(2017, 1, 1, 7, 25, 0), CategoryFixtures.Default.Id, 21.1m);
        public static Transaction Foreign = Transaction.Create("f5c8c2dc-0a75-4693-bbfd-a06915fa1903", AccountFixtures.Foreign.Id, "Foreign", new DateTime(2017, 1, 11, 7, 25, 0), CategoryFixtures.Foreign.Id, 1.1m);
        public static Guid NotExisting = new Guid("42ddaa6f-578f-4b82-b91d-f01918319de7");
    }

    public class Transaction
    {
        public Guid Id { get; set; }
        public Guid AccountId { get; set; }
        public string Description { get; set; }
        public DateTime MovementDate { get; set; }
        public Guid CategoryId { get; set; }
        public decimal Amount { get; set; }

        private Transaction() { }

        public static Transaction Create(string id, Guid accountId, string description, DateTime movementDate, Guid categoryId, decimal amount)
        {
            return new Transaction()
            {
                Id = new Guid(id),
                AccountId = accountId,
                Description = description,
                MovementDate = movementDate,
                CategoryId = categoryId,
                Amount = amount
            };
        }
    }
}
