﻿using System;

namespace MyWallet.Api.Tests.Fixtures
{
    public static class AccountFixtures
    {
        public static Account ExistingAccount_Cash { get; set; } = new Account(
            "7df13f65-a9ba-4e10-bb04-7d17f779ee22",
            UserFixtures.Existing.Guid,
            "Cash Account");

        public static Account ExistingAccount_Savings { get; set; } = new Account(
            "35ba03f1-101b-4c59-84e9-3a6a33091958",
            UserFixtures.Existing.Guid,
            "Fio");

        public static Account ExistingAccount_ToUpdate { get; set; } = new Account(
            "7a323618-e67d-40a9-8cca-6bf57f45c300",
            UserFixtures.Existing.Guid,
            "to update");

        public static Account ExistingAccount_ToDelete { get; set; } = new Account(
            "0535c8fc-a3a2-4251-8178-1468876921c5",
            UserFixtures.Existing.Guid,
            "to delete");

        public static Account Foreign { get; set; } = new Account(
            "B4A99CF8-2263-4104-AF33-3158DA61F3EA",
            UserFixtures.Peter.Guid,
            "Peters Account");

        public static Guid NotExisting = new Guid("dad330a5-ee18-4b59-8816-88a62905117b");
    }

    public class Account
    {
        public Account(string guidString, Guid userId, string name)
        {
            this.Id = new Guid(guidString);
            this.UserId = userId;
            this.Name = name;
        }

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
    }
}
