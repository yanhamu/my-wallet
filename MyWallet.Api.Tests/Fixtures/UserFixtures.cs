﻿using System;

namespace MyWallet.Api.Tests.Fixtures
{
    public static class UserFixtures
    {
        public static User Existing = new User("Tomas", "password", "F927DC85-8458-4FDD-AC64-BD2F6E3EC4AF");
        public static User Peter = new User("Peter", "password", "B80F620B-BC96-4FF6-AD4F-D963EAD7C384");
    }

    public class User
    {
        public User(string username, string password, string guidString)
        {
            this.Username = username;
            this.Password = password;
            this.Guid = new Guid(guidString);
        }

        public string Username { get; private set; }
        public string Password { get; private set; }
        public Guid Guid { get; set; }
    }
}