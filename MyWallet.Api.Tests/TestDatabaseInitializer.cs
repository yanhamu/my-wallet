﻿using Castle.Windsor;
using MyWallet.DataAccess;
using System.Data.Entity;

namespace MyWallet.Api.Tests
{
    public static class TestDatabaseInitializer
    {
        public static void Initialize(IWindsorContainer dependencyContainer)
        {
            Database.SetInitializer(new TestDatabaseInitializerStrategy());

            var context = dependencyContainer.Resolve<MyWalletDbContext>();
            context.Database.Initialize(true);
        }
    }

    public class TestDatabaseInitializerStrategy : DropCreateDatabaseAlways<MyWalletDbContext> { }
}
