﻿using System.Net.Http;
using System.Net.Http.Formatting;

namespace Microsoft.Owin.Testing
{
    public static class TestServerExtensions
    {
        public static RequestBuilder CreateJsonRequest<T>(this TestServer server, string url, T data)
        {
            return server
                .CreateRequest(url)
                .AddHeader("Content-type", "application/json")
                .And(r => r.Content = new ObjectContent<T>(data, new JsonMediaTypeFormatter()));
        }
        public static RequestBuilder CreateJsonRequest(this TestServer server, string url)
        {
            return server
                .CreateRequest(url)
                .AddHeader("Content-type", "application/json");
        }
    }

    public class ResponseJson
    {
        public string Access_Token { get; set; }
        public string Token_Type { get; set; }
        public int Expires_In { get; set; }
    }
}
