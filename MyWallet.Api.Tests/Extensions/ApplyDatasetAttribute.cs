﻿using NDbUnit.Core.SqlClient;
using System;
using System.Configuration;
using System.Reflection;
using Xunit.Sdk;

namespace MyWallet.Api.Tests.Extensions
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ApplyDatasetAttribute : BeforeAfterTestAttribute
    {
        private readonly string dataset;

        public ApplyDatasetAttribute(string dataset)
        {
            this.dataset = dataset;
        }

        public override void Before(MethodInfo methodUnderTest)
        {
            var mySqlDatabase = new SqlDbUnitTest(ConfigurationManager.ConnectionStrings["MyWallet"].ConnectionString);
            mySqlDatabase.ReadXmlSchema($@"..\..\Datasets\{dataset}.xsd");
            mySqlDatabase.ReadXml($@"..\..\Datasets\{dataset}.xml");
            mySqlDatabase.PerformDbOperation(NDbUnit.Core.DbOperationFlag.CleanInsertIdentity);
        }
    }
}
