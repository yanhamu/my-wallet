﻿using Common.Library.UrlHelper;
using Microsoft.Owin.Testing;
using MyWallet.Api.Tests.Fixtures;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyWallet.Api.Tests.Extensions
{
    public sealed class TestServerWrapper : IDisposable
    {
        public TestServer Server { get; private set; }
        private UrlBuilder urlBuilder;
        private bool isAuthorized = false;

        private TestServerWrapper(TestServer testServer, UrlBuilder urlBuilder)
        {
            this.Server = testServer;
            this.urlBuilder = urlBuilder;
        }

        public static TestServerWrapper CreateClient<TStartup>(UrlBuilder urlBuilder)
        {
            var testServer = TestServer.Create<TStartup>();
            return new TestServerWrapper(testServer, urlBuilder);
        }

        public TestServerWrapper IsAuthorized()
        {
            this.isAuthorized = true;
            return this;
        }

        public TestServerWrapper IsUnauthorized()
        {
            this.isAuthorized = false;
            return this;
        }

        public async Task<HttpResponseMessage> List()
        {
            var builder = await GetBaseRequest(urlBuilder.GetUrl());
            return await builder.GetAsync();
        }

        public async Task<HttpResponseMessage> Show(params Guid[] guid)
        {
            var builder = await GetBaseRequest(urlBuilder.GetUrl(guid));
            return await builder.GetAsync();
        }

        public async Task<HttpResponseMessage> UpdateAsync<T>(T data)
        {
            var builder = await GetBaseRequest(urlBuilder.GetUrl(), data);
            return await builder.SendAsync("PUT");
        }

        public async Task<HttpResponseMessage> UpdateAsync<T>(T data, params Guid[] ids)
        {
            var builder = await GetBaseRequest(urlBuilder.GetUrl(ids), data);
            return await builder.SendAsync("PUT");
        }

        public async Task<HttpResponseMessage> CreateAsync<T>(T data)
        {
            var builder = await GetBaseRequest(urlBuilder.GetUrl(), data);
            return await builder.SendAsync("POST");
        }

        public async Task<HttpResponseMessage> CreateAsync<T>(T data, params Guid[] ids)
        {
            var builder = await GetBaseRequest(urlBuilder.GetUrl(ids), data);
            return await builder.SendAsync("POST");
        }

        public async Task<HttpResponseMessage> DeleteAsync(params Guid[] ids)
        {
            var builder = await GetBaseRequest(urlBuilder.GetUrl(ids));
            return await builder.SendAsync("DELETE");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private async Task<RequestBuilder> GetBaseRequest(string url)
        {
            var request = this
                .Server
                .CreateJsonRequest(url);

            if (isAuthorized)
            {
                var token = await GetSecurityToken(Server);
                request = request.AddHeader("Authorization", $"bearer {token}");
            }
            return request;
        }

        private async Task<RequestBuilder> GetBaseRequest<T>(string url, T data)
        {
            var request = this
                .Server
                .CreateJsonRequest(url, data);

            if (isAuthorized)
            {
                var token = await GetSecurityToken(Server);
                request = request.AddHeader("Authorization", $"bearer {token}");
            }
            return request;
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
                this.Server.Dispose();

            this.Server = null;
        }

        private async Task<string> GetSecurityToken(TestServer server)
        {
            var body = $"username={UserFixtures.Existing.Username}&password={UserFixtures.Existing.Password}&grant_type=password";
            var response = server
                .CreateRequest("api/getsecuretoken")
                .AddHeader("Content-Type", "application/x-www-form-urlencoded")
                .And(r => r.Content = new StringContent(body))
                .PostAsync();

            var t = await response.ReadContent<ResponseJson>();
            return t.Access_Token;
        }
    }
}