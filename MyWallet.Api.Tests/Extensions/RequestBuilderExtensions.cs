﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Microsoft.Owin.Testing
{
    public static class RequestBuilderExtensions
    {
        public async static Task<T> ReadContent<T>(this Task<HttpResponseMessage> response)
        {
            var r = await response;
            return FromJson<T>(r.Content.ReadAsStringAsync().Result);
        }

        private static T FromJson<T>(string obj)
        {
            return JsonConvert.DeserializeObject<T>(obj);
        }
    }
}