﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace MyWallet.Api.Tests.Extensions
{
    public static class AssertionExtensions
    {
        public static HttpResponseMessage AssertStatusCode(this HttpResponseMessage response, HttpStatusCode expected)
        {
            Assert.Equal(expected, response.StatusCode);
            return response;
        }

        public static async Task<HttpResponseMessage> AssertStatusCode(this Task<HttpResponseMessage> response, HttpStatusCode expected)
        {
            var r = await response;
            Assert.Equal(expected, r.StatusCode);
            return r;
        }
    }
}
