﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MyWallet.App_Start.DiInstallers;
using MyWallet.DataAccess;

namespace MyWallet.Api.Tests
{
    public class TestBootstrapper
    {
        public static IWindsorContainer Create()
        {
            var container = new WindsorContainer();

            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));

            container.Install(new AutoMapperInstaller());
            container.Install(new CommandHandlersInstaller());
            container.Install(new ControllerInstaller());
            container.Install(new TestDbContextInstaller());
            container.Install(new DispatcherInstaller());
            container.Install(new ProviderInstaller());
            container.Install(new QueryServicesInstaller());
            container.Install(new RepositoryInstaller());
            container.Install(new ServiceInstaller());
            return container;
        }
    }

    public class TestDbContextInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<MyWalletDbContext>());
        }
    }
}
