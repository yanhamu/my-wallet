﻿using Common.Library.UrlHelper;
using MyWallet.Api.Tests.Extensions;
using MyWallet.Api.Tests.Fixtures;
using System;
using System.Net;
using Xunit;

namespace MyWallet.Api.Tests.Controllers.Categories
{
    [ApplyDataset("Users")]
    [ApplyDataset("Categories")]
    public class CategoryControllerTests : IDisposable
    {
        private TestServerWrapper client;

        public CategoryControllerTests()
        {
            var urlBuilder = new UrlBuilder("api")
                .AddUrlSegment("categories")
                .AddPlaceholder();

            this.client = TestServerWrapper
                .CreateClient<TestStartup>(urlBuilder)
                .IsAuthorized();
        }

        [Fact]
        public async void Should_List_Categories()
        {
            await client
                .List()
                .AssertStatusCode(HttpStatusCode.OK);
        }

        [Fact]
        public async void Should_Show_Category()
        {
            await client
                .Show(CategoryFixtures.Default.Id)
                .AssertStatusCode(HttpStatusCode.OK);
        }

        [Fact]
        public async void Should_Update_Category()
        {
            var category = new Model.Category()
            {
                Color = "550055",
                IsDefault = true,
                Name = "new-name"
            };

            await client
                .UpdateAsync(category, CategoryFixtures.ToUpdate.Id)
                .AssertStatusCode(HttpStatusCode.OK);
        }

        [Fact]
        public async void Should_Remove_Category()
        {
            await client
                .DeleteAsync(CategoryFixtures.ToDelete.Id)
                .AssertStatusCode(HttpStatusCode.NoContent);
        }

        [Fact]
        public async void Should_Create_New_Category()
        {
            var category = new Model.Category()
            {
                Color = "550055",
                IsDefault = true,
                Name = "new name"
            };

            await client
                .CreateAsync(category)
                .AssertStatusCode(HttpStatusCode.OK);
        }

        [Fact]
        public async void Should_Return_404_When_NonExisting_CategoryId_Is_Get()
        {
            await client
                .Show(new Guid("3c4627ec-39c3-4d15-8077-5c3e259af7c7"))
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_Foreign_CategoryId_Is_Get()
        {
            await client
                .Show(CategoryFixtures.Foreign.Id)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_Foreign_CategoryId_Is_Updated()
        {
            var category = new Model.Category()
            {
                Color = "550055",
                IsDefault = true,
                Name = "new-name"
            };

            await client
                .UpdateAsync(category, CategoryFixtures.Foreign.Id)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_400_When_Invadid_Category_Is_Updated()
        {
            var category = new Model.Category()
            {
                Color = "550055",
                IsDefault = true,
                Name = null
            };

            await client
                .UpdateAsync(category, CategoryFixtures.ToUpdate.Id)
                .AssertStatusCode(HttpStatusCode.BadRequest);
        }

        //TODO unit tests model validation
        //TODO test updating not updatable fields
        [Fact]
        public async void Should_Return_404_When_Removing_Foreign_Category()
        {
            await client
                .DeleteAsync(CategoryFixtures.Foreign.Id)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_400_When_Creating_Invalid_Category()
        {
            var category = new Model.Category()
            {
                Color = "550055",
                IsDefault = true,
                Name = null
            };

            await client
                .CreateAsync(category)
                .AssertStatusCode(HttpStatusCode.BadRequest);
        }

        public void Dispose()
        {
            this.client.Dispose();
        }
    }
}