﻿using Common.Library.UrlHelper;
using MyWallet.Api.Model;
using MyWallet.Api.Tests.Extensions;
using System;
using System.Net;
using Xunit;

namespace MyWallet.Api.Tests.Controllers.Accounts
{
    [ApplyDataset("Users")]
    [ApplyDataset("FioAccounts")]
    public class FioAccountControllerTests : IDisposable
    {
        private TestServerWrapper client;

        public FioAccountControllerTests()
        {
            var urlBuilder = new UrlBuilder("api")
                .AddUrlSegment("accounts")
                .AddUrlSegment("fio");

            this.client = TestServerWrapper
                .CreateClient<TestStartup>(urlBuilder)
                .IsAuthorized();
        }

        [Fact]
        public async void Should_Create_New_FioAccount()
        {
            await client
                .CreateAsync(new FioAccount() { Name = "my fioo", Token = "1234token" })
                .AssertStatusCode(HttpStatusCode.OK);
        }

        [Fact]
        public async void Should_Return_400_When_Invalid_FioAccount_Is_Passed()
        {
            await client
                .CreateAsync(new FioAccount())
                .AssertStatusCode(HttpStatusCode.BadRequest);
        }

        //TODO test other cases

        public void Dispose()
        {
            this.client.Dispose();
        }
    }
}
