﻿using Common.Library.UrlHelper;
using MyWallet.Api.Model;
using MyWallet.Api.Tests.Extensions;
using System;
using System.Net;
using Xunit;

namespace MyWallet.Api.Tests.Controllers.Accounts
{
    [ApplyDataset("FioAccounts")]
    [ApplyDataset("Users")]
    [ApplyDataset("Accounts")]
    public class AccountControllerTests : IDisposable
    {
        private TestServerWrapper client;

        public AccountControllerTests()
        {
            var urlBuilder = new UrlBuilder("api")
                .AddUrlSegment("accounts")
                .AddPlaceholder();

            this.client = TestServerWrapper
                .CreateClient<TestStartup>(urlBuilder)
                .IsAuthorized();
        }

        [Fact]
        public async void Should_List_Accounts()
        {
            await client
                .List()
                .AssertStatusCode(HttpStatusCode.OK);
        }

        [Fact]
        public async void Should_Show_Account()
        {
            await client
                .Show(Fixtures.AccountFixtures.ExistingAccount_Cash.Id)
                .AssertStatusCode(HttpStatusCode.OK);
        }

        [Fact]
        public async void Should_Create_New_Account()
        {
            var toCreate = new Account() { Name = "My third account" };

            await client
                .CreateAsync(toCreate)
                .AssertStatusCode(HttpStatusCode.OK);
        }

        [Fact]
        public async void Should_Update_Account()
        {
            var toUpdate = new Account() { Name = "renamed account" };

            await client
                .UpdateAsync(toUpdate, Fixtures.AccountFixtures.ExistingAccount_ToUpdate.Id)
                .AssertStatusCode(HttpStatusCode.OK);
        }

        [Fact]
        public async void Should_Remove_Account()
        {
            await client
                .DeleteAsync(Fixtures.AccountFixtures.ExistingAccount_ToDelete.Id)
                .AssertStatusCode(HttpStatusCode.NoContent);
        }

        [Fact]
        public async void Should_Return_404_When_Foreign_Account_Id_Is_Passed()
        {
            await client
                .UpdateAsync(new Account() { Name = "test" }, Fixtures.AccountFixtures.Foreign.Id)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_Invalid_Account_Is_Created()
        {
            await client.CreateAsync(new Account())
                .AssertStatusCode(HttpStatusCode.BadRequest);

            await client.CreateAsync(default(Account))
                .AssertStatusCode(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async void Should_Return_404_When_Updating_Invalid_Account()
        {
            await client
                .UpdateAsync(new Account(), Fixtures.AccountFixtures.ExistingAccount_Cash.Id)
                .AssertStatusCode(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async void Should_Return_404_When_Updating_NonExisting_Account()
        {
            await client
                .UpdateAsync(new Account() { Name = "my account" }, Fixtures.AccountFixtures.NotExisting)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_Removing_NonExisting_Account()
        {
            await client
                .DeleteAsync(Fixtures.AccountFixtures.NotExisting)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_Getting_Foreign_Account()
        {
            await client
                .Show(Fixtures.AccountFixtures.Foreign.Id)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_Deleting_Foreign_Account()
        {
            await client
                .DeleteAsync(Fixtures.AccountFixtures.Foreign.Id)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        public void Dispose()
        {
            this.client.Dispose();
        }
    }
}