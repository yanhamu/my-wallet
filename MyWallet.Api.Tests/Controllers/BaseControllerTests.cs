﻿using Microsoft.Owin.Testing;
using System;
using Xunit;

[assembly: CollectionBehavior(DisableTestParallelization = true)]
namespace MyWallet.Api.Tests.Controllers
{
    public class BaseControllerTests : IDisposable
    {
        protected TestServer server;
        private readonly string baseUrl;

        public BaseControllerTests(string baseUrl)
        {
            this.server = TestServer.Create<TestStartup>();

            if (string.IsNullOrWhiteSpace(baseUrl))
                throw new ArgumentException(nameof(baseUrl));

            this.baseUrl = baseUrl;
        }

        public virtual string GetUrl()
        {
            return baseUrl;
        }

        public virtual string GetUrl(string id)
        {
            return baseUrl + "/" + id;
        }


        public virtual string GetUrl(Guid id)
        {
            return baseUrl + "/" + id.ToString();
        }

        public virtual void Dispose()
        {
            this.server.Dispose();
        }
    }
}
