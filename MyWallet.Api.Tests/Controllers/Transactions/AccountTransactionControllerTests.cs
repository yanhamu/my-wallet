﻿using Common.Library.UrlHelper;
using MyWallet.Api.Tests.Extensions;
using MyWallet.Api.Tests.Fixtures;
using System;
using System.Net;
using Xunit;

namespace MyWallet.Api.Tests.Controllers.Transactions
{
    [ApplyDataset("Users")]
    [ApplyDataset("Accounts")]
    [ApplyDataset("Categories")]
    [ApplyDataset("Transactions")]
    public class AccountTransactionControllerTests : IDisposable
    {
        private TestServerWrapper client;

        public AccountTransactionControllerTests()
        {
            var urlBuilder = new UrlBuilder("api")
                .AddUrlSegment("accounts")
                .AddPlaceholder()
                .AddUrlSegment("transactions")
                .AddPlaceholder();

            this.client = TestServerWrapper
                .CreateClient<TestStartup>(urlBuilder)
                .IsAuthorized();
        }

        [Fact]
        public async void Should_Show_AccountTransactions()
        {
            await client
                .Show(AccountFixtures.ExistingAccount_Cash.Id)
                .AssertStatusCode(HttpStatusCode.OK);
        }

        [Fact]
        public async void Should_Return_404_When_NonExisting_AccountId_Is_Passed()
        {
            await client
                .Show(AccountFixtures.NotExisting)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_Foreign_AccountId_Is_Passed()
        {
            await client
                .Show(AccountFixtures.Foreign.Id)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_NonExisting_TransactionId_Is_Passed()
        {
            await client
                .Show(AccountFixtures.ExistingAccount_Cash.Id, TransactionFixtures.NotExisting)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_Foreign_AccountId_And_TransactionId_Is_Passed()
        {
            await client
                .Show(AccountFixtures.Foreign.Id, TransactionFixtures.Foreign.Id)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_Not_Existing_AccountId_And_TransactionId_Is_Passed()
        {
            await client
                .Show(AccountFixtures.NotExisting, TransactionFixtures.Existing.Id)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Should_Return_404_When_Inconsistent_TransactionId_And_AccountId_Is_Passed()
        {
            await client
                .Show(AccountFixtures.ExistingAccount_Savings.Id, TransactionFixtures.Existing.Id)
                .AssertStatusCode(HttpStatusCode.NotFound);
        }


        [Fact]
        public async void Should_Create_New_Account_Transaction()
        {
            var accountTransaction = new Model.Transaction()
            {
                AccountId = TransactionFixtures.Existing.AccountId,
                Amount = 10m,
                CategoryId = CategoryFixtures.Default.Id,
                Description = "myDescription",
                MovementDate = DateTime.Now
            };
            await client.CreateAsync(accountTransaction, TransactionFixtures.Existing.AccountId)
                .AssertStatusCode(HttpStatusCode.OK);
        }

        public void Dispose()
        {
            this.client.Dispose();
        }
    }
}