﻿using Common.Library.UrlHelper;
using MyWallet.Api.Tests.Extensions;
using System;
using System.Net;
using Xunit;

namespace MyWallet.Api.Tests.Controllers.Transactions
{
    [ApplyDataset("Users")]
    [ApplyDataset("Accounts")]
    [ApplyDataset("Transactions")]
    public class TransactionControllerTests : IDisposable
    {
        private TestServerWrapper client;

        public TransactionControllerTests()
        {
            var urlBuilder = new UrlBuilder("api")
                .AddUrlSegment("transactions")
                .AddPlaceholder();

            this.client = TestServerWrapper
                .CreateClient<TestStartup>(urlBuilder)
                .IsAuthorized();
        }

        [Fact]
        public async void Should_List_Transactions()
        {
            await client
                .List()
                .AssertStatusCode(HttpStatusCode.OK);
        }
        //TODO assert other cases
        
        public void Dispose()
        {
            this.client.Dispose();
        }
    }
}
