﻿using Common.Library.UrlHelper;
using Microsoft.Owin.Testing;
using MyWallet.Api.Tests.Extensions;
using MyWallet.Api.Tests.Fixtures;
using MyWallet.Controllers.Users;
using MyWallet.Service.Authentication;
using System.Net;
using System.Net.Http;
using Xunit;

namespace MyWallet.Api.Tests.Controllers.Users
{
    [ApplyDataset("Users")]
    public class UserControllerTests
    {
        private TestServerWrapper client;

        public UserControllerTests()
        {
            var urlBuilder = new UrlBuilder("api")
                .AddUrlSegment("users");

            this.client = TestServerWrapper.CreateClient<TestStartup>(urlBuilder);
        }

        [Fact]
        public async void Should_Create_New_User()
        {
            var username = "newUser";
            var password = "newPassword";

            var data = new CreateUserDefaultAuthenticationCommand(username, password);

            await client
                .IsUnauthorized()
                .CreateAsync(data)
                .AssertStatusCode(HttpStatusCode.OK);

            client.IsAuthorized();
        }

        [Fact]
        public async void Should_return_security_token()
        {
            var body = $"username={UserFixtures.Existing.Username}&password={UserFixtures.Existing.Password}&grant_type=password";

            var response = client
                .Server
                .CreateRequest("api/getsecuretoken")
                .AddHeader("Content-Type", "application/x-www-form-urlencoded")
                .And(r => r.Content = new StringContent(body))
                .PostAsync()
                .AssertStatusCode(HttpStatusCode.OK);

            var content = await response.ReadContent<ResponseJson>();

            Assert.NotNull(content.Access_Token);
        }

        [Fact]
        public async void Should_Return_400_When_Wrong_Username_Or_Password_Is_Passed()
        {
            var wrongPassword = "xxxxx";
            var body = $"username={UserFixtures.Existing.Username}&password={wrongPassword}&grant_type=password";

            var response = await client
                .Server
                .CreateRequest("api/getsecuretoken")
                .AddHeader("Content-Type", "application/x-www-form-urlencoded")
                .And(r => r.Content = new StringContent(body))
                .PostAsync()
                .AssertStatusCode(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async void Should_Return_409_When_Passed_Username_Already_Exist()
        {
            var data = new CreateUserDefaultAuthenticationCommand(UserFixtures.Existing.Username, "xxx");
            await client
                .CreateAsync(data)
                .AssertStatusCode(HttpStatusCode.Conflict);
        }

        [Fact]
        public async void Should_Return_400_When_Null_Argument_Is_Passed()
        {
            await client
                .CreateAsync(default(CreateUserModel))
                .AssertStatusCode(HttpStatusCode.BadRequest);
        }

        [InlineData(default(string), "asdf")]
        [InlineData(default(string), default(string))]
        [InlineData("asdf", default(string))]
        [InlineData("", "")]
        [Theory]
        public async void Should_Return_400_When_Invalid_Argument_Is_Passed(string username, string password)
        {
            await client
                .CreateAsync(new CreateUserModel(username, password))
                .AssertStatusCode(HttpStatusCode.BadRequest);
        }
    }
}