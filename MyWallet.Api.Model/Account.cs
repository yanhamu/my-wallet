﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace MyWallet.Api.Model
{
    public class Account
    {
        public Guid? Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
