﻿using System;

namespace MyWallet.Api.Model
{
    public class Transaction : TransactionHeader
    {
        public string Description { get; set; }
        public DateTime MovementDate { get; set; }
    }
}
