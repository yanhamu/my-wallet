﻿using System;

namespace MyWallet.Api.Model
{
    public class TransactionHeader
    {
        public Guid? Id { get; set; }
        public Guid AccountId { get; set; }
        public decimal Amount { get; set; }
        public Guid? CategoryId { get; set; }
        public bool WasSpotted { get; set; }
    }
}