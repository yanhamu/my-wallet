﻿using System.Collections.Generic;

namespace MyWallet.Api.Model
{
    public class HierarchicalCategory : Category
    {
        public List<HierarchicalCategory> Children { get; set; }

        public HierarchicalCategory()
        {
            Children = new List<HierarchicalCategory>();
        }
    }
}
