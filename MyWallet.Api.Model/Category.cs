﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MyWallet.Api.Model
{
    public class Category
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Color { get; set; }
        public bool IsDefault { get; set; }
    }
}