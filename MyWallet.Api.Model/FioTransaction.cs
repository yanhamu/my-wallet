﻿namespace MyWallet.Api.Model
{
    public class FioTransaction : Transaction
    {
        public long MovementId { get; set; }
        public string Currency { get; set; }
        public string ContraAccount { get; set; }
        public string ContraAccountName { get; set; }
        public string ContraAccountBankCode { get; set; }
        public string ContraAccountBankName { get; set; }
        public int? ConstantSymbol { get; set; }
        public long? VariableSymbol { get; set; }
        public long? SpecificSymbol { get; set; }
        public string UserIdentification { get; set; }
        public string MessageForReceiver { get; set; }
        public string TransactionType { get; set; }
        public string TransactionExecutive { get; set; }
        public string Specification { get; set; }
        public string Comment { get; set; }
        public string BankIndentifierCode { get; set; }
        public long? TransactionId { get; set; }
    }
}
