﻿using System;

namespace MyWallet.Api.Model
{
    public class FioAccount : Account
    {
        public string Token { get; set; } //TODO consider safety
        public DateTime? LastSyncDate { get; set; }
    }
}
