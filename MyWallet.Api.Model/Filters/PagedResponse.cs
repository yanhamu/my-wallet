﻿using System.Collections.Generic;

namespace MyWallet.Api.Model.Filters
{
    public class PagedResponse<T>
    {
        public int Page { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public IEnumerable<T> Data { get; private set; }
        public int TotalRecords { get; private set; }

        public PagedResponse(IEnumerable<T> data, int page, int totalRecords, int pageSize)
        {
            this.Data = data;
            this.Page = page;
            this.PageSize = pageSize;
            this.TotalRecords = totalRecords;
            this.TotalPages = (totalRecords / pageSize) + ((totalRecords % pageSize) != 0 ? 1 : 0);
        }
    }
}
