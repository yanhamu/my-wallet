﻿namespace MyWallet.Api.Model.Filters
{
    public class PageFilter
    {
        public int? Page { get; set; }
        public int? PageSize { get; set; }

        public PageFilter()
        {
            Page = 1;
            PageSize = 20;
        }
    }
}
