﻿using System;

namespace MyWallet.Api.Model.Filters
{
    public class TransactionFilter : PageFilter
    {
        public Guid? CategoryId { get; set; }
        public bool? Spotted { get; set; }
    }
}
