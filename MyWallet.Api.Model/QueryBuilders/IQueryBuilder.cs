﻿using System.Linq;

namespace MyWallet.Api.Model.QueryBuilders
{
    public interface IQueryBuilder<TFilter, TObject> where TFilter : class
    {
        IQueryable<TObject> Filter(TFilter filter, IQueryable<TObject> query);
    }
}
