﻿using Common.Library.UrlHelper;
using System;
using Xunit;

namespace Common.Library.Tests.UrlHelper
{
    public class UrlBuilderTests
    {
        [Fact]
        public void Should_Return_Base_Url()
        {
            var baseUrl = "api/accounts";
            var builder = new UrlBuilder(baseUrl);
            Assert.Equal(baseUrl, builder.GetUrl());
        }

        [Fact]
        public void Should_Return_Base_Url_From_Multiple_Parts()
        {
            var builder = new UrlBuilder("api");

            builder.AddUrlSegment("accounts");

            Assert.Equal("api/accounts", builder.GetUrl());
        }

        [Fact]
        public void Should_Return_Url_With_Replaced_Placeholder()
        {
            var builder = new UrlBuilder("api")
                .AddUrlSegment("accounts")
                .AddPlaceholder();

            var accountId = new Guid("f2140aae-7ed7-4c33-b236-fb7bd87996f8");

            Assert.Equal("api/accounts/f2140aae-7ed7-4c33-b236-fb7bd87996f8", builder.GetUrl(accountId));
        }

        [Fact]
        public void Should_Return_Url_With_Partially_Replaced_Placeholder()
        {
            var builder = new UrlBuilder("api")
                .AddUrlSegment("accounts")
                .AddPlaceholder()
                .AddUrlSegment("transactions")
                .AddPlaceholder();

            var accountId = new Guid("f2140aae-7ed7-4c33-b236-fb7bd87996f8");

            Assert.Equal("api/accounts/f2140aae-7ed7-4c33-b236-fb7bd87996f8/transactions", builder.GetUrl(accountId));
        }

        [Fact]
        public void Should_Return_Url_With_Multiple_Placeholders()
        {
            var accountId = new Guid("73b072e6-a1d1-4973-bc62-2d31d435a1e0");
            var transactionId = new Guid("f2140aae-7ed7-4c33-b236-fb7bd87996f8");

            var builder = new UrlBuilder("api")
                .AddUrlSegment("accounts")
                .AddPlaceholder()
                .AddUrlSegment("transactions")
                .AddPlaceholder();

            var expectedUrl = "api/accounts/73b072e6-a1d1-4973-bc62-2d31d435a1e0/transactions/f2140aae-7ed7-4c33-b236-fb7bd87996f8";

            Assert.Equal(expectedUrl, builder.GetUrl(accountId, transactionId));
        }

        [Fact]
        public void Should_Return_Url_Without_Placeholders()
        {
            var builder = new UrlBuilder("api")
                .AddUrlSegment("accounts")
                .AddPlaceholder();

            var expectedUrl = "api/accounts";

            Assert.Equal(expectedUrl, builder.GetUrl());
        }
    }
}
